﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dealership.Data.Services;
using Dealership.Models;

namespace Dealership.Service.Services
{
    public class VehicleService : IVehicleService
    {
        private readonly IDataService _dataService;
        private readonly IUserService _userservice;

        public VehicleService(IDataService dataService, IUserService userService)
        {
            _dataService = dataService;
            _userservice = userService;
        }

        public async Task<IList<Vehicle>> GetVehiclesAsync()
        {
            return await _dataService.GetVehiclesAsync();
        }

        public async Task<Vehicle> GetVehicleAsync(int Id)
        {
            return await _dataService.GetVehicleAsync(Id);
        }

        public async Task<Vehicle> SaveNewAsync(Vehicle vehicle)
        {
            // await _userservice.ValidateUserRoleAsync(UserId, RoleType.Admin);
            ValidateVehicle(vehicle);
            return await _dataService.SaveNewVehicleAsync(vehicle);
        }

        public async Task<Vehicle> UpdateAsync(Vehicle vehicle)
        {
            ValidateVehicle(vehicle, true);
            return await _dataService.UpdateVehicleAsync(vehicle);
        }

        public async Task<Vehicle> DeleteAsync(int Id)
        {
            return await _dataService.DeleteVehicleAsync(Id);
        }

        public async Task<IList<VehicleModel>> GetVehicleMakeModelsAsync(int makeId)
        {
            return await _dataService.GetVehicleMakeModelsAsync(makeId);
        }

        public async Task<IList<VehicleMake>> GetVehicleMakesAsync()
        {
            return await _dataService.GetVehicleMakesAsync();
        }

        private void ValidateVehicle(Vehicle vehicle, bool requiresId = false)
        {
            if (vehicle == null)
            {
                throw new NullReferenceException("Vehicle can not be null.");
            }
            if (requiresId && vehicle.Id < 0)
            {
                throw new Exception("Vehicle Id is incorrect");
            }
            if (vehicle.ModelId < 0)
            {
                throw new Exception("Vehicle model Id is incorrect");
            }
            if (vehicle.RegNumber == null || string.IsNullOrEmpty(vehicle.RegNumber))
            {
                throw new NullReferenceException("Vehicle registration number is required");
            }
        }
    }
}