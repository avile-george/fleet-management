﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dealership.Models;

namespace Dealership.Service.Services
{
    public interface IVehicleService
    {
        Task<Vehicle> DeleteAsync(int Id);

        Task<Vehicle> GetVehicleAsync(int Id);

        Task<IList<Vehicle>> GetVehiclesAsync();

        Task<Vehicle> SaveNewAsync(Vehicle vehicle);

        Task<Vehicle> UpdateAsync(Vehicle vehicle);

        Task<IList<VehicleModel>> GetVehicleMakeModelsAsync(int makeId);

        Task<IList<VehicleMake>> GetVehicleMakesAsync();
    }
}