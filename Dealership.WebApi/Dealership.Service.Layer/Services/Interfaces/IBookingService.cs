﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dealership.Models;

namespace Dealership.Service.Services
{
    public interface IBookingService
    {
        Task<Booking> AddNewBookingAsync(Booking booking);
        Task<IList<Booking>> GetBookingsAsync();
        Task<Booking> UpdateBookingAsync(Booking booking);
    }
}