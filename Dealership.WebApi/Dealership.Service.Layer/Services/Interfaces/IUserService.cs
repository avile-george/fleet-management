﻿using System.Threading.Tasks;
using Dealership.Models;

namespace Dealership.Service.Services
{
    public interface IUserService
    {
        Task<User> GetUserAsync(int Id);

        Task<User> GetUserAsync(string username, string password);

        Task<bool> ValidateUserRoleAsync(int userId, RoleType expectedRole);
    }
}