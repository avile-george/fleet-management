﻿using System.Threading.Tasks;
using Dealership.Data.Services;
using Dealership.Models;

namespace Dealership.Service.Services
{
    public class UserService : IUserService
    {
        private readonly IDataService _dataService;
        private readonly IIdentityService _identityService;

        public UserService(IDataService dataService, IIdentityService identityService)
        {
            _dataService = dataService;
            _identityService = identityService;
        }

        public async Task<User> GetUserAsync(string Id)
        {
            return await _identityService.GetUserAsync(Id);
        }

        public async Task<bool> ValidateUserRoleAsync(string userId, RoleType expectedRole)
        {
            //User user = await GetUserAsync(userId);
            //string adminId = _
            //// Assuming Admin is permitted to perform all operations
            //if (user == null || (user.RoleId != (int)RoleType.Admin && user.RoleId != (int)expectedRole))
            //{
            //    throw new UnauthorizedAccessException("User has insufficient/ incorrect permissions to compete this action");
            //}

            return true;
        }
    }
}