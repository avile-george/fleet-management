﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dealership.Data.Services;
using Dealership.Models;

namespace Dealership.Service.Services
{
    public class BookingService : IBookingService
    {
        private readonly IDataService _dataService;
        private readonly IUserService _userservice;

        public BookingService(IDataService dataService, IUserService userService)
        {
            _dataService = dataService;
            _userservice = userService;
        }

        public async Task<IList<Booking>> GetBookingsAsync()
        {
            return await _dataService.GetBookingsAsync();
        }

        public async Task<Booking> UpdateBookingAsync(Booking booking)
        {
            // await _userservice.ValidateUserRoleAsync(booking.UserId, RoleType.User);
            ValidateBooking(booking, true);
            return await _dataService.UpdateBookingAsync(booking);
        }

        public async Task<Booking> AddNewBookingAsync(Booking booking)
        {
            // await _userservice.ValidateUserRoleAsync(booking.UserId, RoleType.User);
            ValidateBooking(booking);
            return await _dataService.AddNewBookingAsync(booking);
        }

        private void ValidateBooking(Booking booking, bool requiresId = false)
        {
            if (booking == null)
            {
                throw new NullReferenceException("Booking can not be null.");
            }
            if (requiresId && booking.Id < 0)
            {
                throw new Exception("Booking Id is incorrect");
            }
            if (booking.VehicleId < 0)
            {
                throw new Exception("Booking vehicle Id is incorrect");
            }
            if (booking.StartDate == null)
            {
                throw new InvalidOperationException("Booking start date invalid");
            }
            if (booking.EndDate == null)
            {
                throw new InvalidOperationException("Booking end date invalid");
            }
        }
    }
}