﻿using System;
using Dealership.Data;
using Dealership.Data.Entities;
using Dealership.Data.Helpers;
using Dealership.Data.Services;
using Dealership.Service.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Dealership.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DealershipContext>(config =>
            {
                // Ideally, I'd like to use the data project as my Migration Assembly but I am experiencing small issues and I do not have the time to fix right now
                config.UseSqlServer(Configuration.GetConnectionString("MyDealershipConnectionString"), b => b.MigrationsAssembly("Dealership.Api.Layer"));
            });

            services.AddDefaultIdentity<User>(cfg =>
            {
                cfg.User.RequireUniqueEmail = true;
                cfg.Password.RequiredLength = 7;
            })
            .AddEntityFrameworkStores<DealershipContext>()
            .AddDefaultTokenProviders();

            services.ConfigureApplicationCookie(options =>
            {
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.Cookie.Name = "MyDealershipCookieName";
                options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                options.LoginPath = "/Identity/Account/Login";
                options.ReturnUrlParameter = CookieAuthenticationDefaults.ReturnUrlParameter;
                options.SlidingExpiration = true;
            });
            services.AddAuthentication();

            services.AddTransient<Seeder>();
            services.AddScoped<IDataService, DataService>();
            services.AddScoped<IIdentityService, IdentityService>();
            services.AddScoped<IVehicleService, VehicleService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IBookingService, BookingService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        public async void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // global cors policy
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                );

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                //using (var scope = app.ApplicationServices.CreateScope())
                //{
                //    var seeder = scope.ServiceProvider.GetService<Seeder>();
                //    await seeder.Seed();
                //}
            }
            else
            {
                app.UseHsts();
            }
            MapperConfig.Initialize();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc();
        }
    }
}