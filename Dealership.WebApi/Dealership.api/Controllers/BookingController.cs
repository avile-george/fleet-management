﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dealership.Models;
using Dealership.Service.Services;
using Microsoft.AspNetCore.Mvc;

namespace Dealership.Api.Layer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingController : ControllerBase
    {
        private readonly IBookingService _bookingService;

        public BookingController(IBookingService bookingService)
        {
            _bookingService = bookingService;
        }

        // GET api/booking
        [HttpGet]
        public async Task<ApiResult<IList<Booking>>> Get()
        {
            try
            {
                IList<Booking> bookings = await _bookingService.GetBookingsAsync();
                return new ApiResult<IList<Booking>>(bookings);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT api/booking
        [HttpPost("update")]
        public async Task<ApiResult<Booking>> Update([FromBody] Booking booking)
        {
            try
            {
                return new ApiResult<Booking>(await _bookingService.UpdateBookingAsync(booking));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // POST api/booking
        [HttpPost]
        public async Task<ApiResult<Booking>> SaveNew([FromBody] Booking booking)
        {
            try
            {
                return new ApiResult<Booking>(await _bookingService.AddNewBookingAsync(booking));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}