﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Dealership.Models;
using Dealership.Service.Services;
using Microsoft.AspNetCore.Mvc;

namespace Dealership.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    //[Authorize]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleService _vehicleService;

        public VehicleController(IVehicleService vehicleService)
        {
            _vehicleService = vehicleService;
        }

        // GET api/vehicle
        [HttpGet]
        public async Task<ApiResult<IEnumerable<Vehicle>>> Get()
        {
            try
            {
                System.Security.Claims.ClaimsPrincipal user = User;
                return new ApiResult<IEnumerable<Vehicle>>(await _vehicleService.GetVehiclesAsync());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET api/vehicle/5
        [HttpGet("{Id}")]
        public async Task<ApiResult<Vehicle>> Get(int Id)
        {
            try
            {
                return new ApiResult<Vehicle>(await _vehicleService.GetVehicleAsync(Id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET api/vehicle/makes
        [HttpGet("makes")]
        public async Task<ApiResult<IEnumerable<VehicleMake>>> GetMakes()
        {
            try
            {
                return new ApiResult<IEnumerable<VehicleMake>>(await _vehicleService.GetVehicleMakesAsync());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // GET api/vehicle/models/0
        [HttpGet("models/{makeId}")]
        public async Task<ApiResult<IEnumerable<VehicleModel>>> GetModels(int makeId)
        {
            try
            {
                return new ApiResult<IEnumerable<VehicleModel>>(await _vehicleService.GetVehicleMakeModelsAsync(makeId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // POST api/vehicle
        [HttpPost]
        public async Task<ApiResult<Vehicle>> SaveNew([FromBody] Vehicle vehicle)
        {
            try
            {
                return new ApiResult<Vehicle>(await _vehicleService.SaveNewAsync(vehicle));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT api/vehicle
        [HttpPost("update")]
        public async Task<ApiResult<Vehicle>> Update([FromBody] Vehicle vehicle)
        {
            try
            {
                return new ApiResult<Vehicle>(await _vehicleService.UpdateAsync(vehicle));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // DELETE api/vehicle/5
        [HttpGet("delete/{Id}")]
        public async Task<ApiResult<Vehicle>> Delete(int Id)
        {
            try
            {
                return new ApiResult<Vehicle>(await _vehicleService.DeleteAsync(Id));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}