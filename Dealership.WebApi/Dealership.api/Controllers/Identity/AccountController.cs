﻿using System.Threading.Tasks;
using Dealership.Models;
using Microsoft.AspNetCore.Mvc;

namespace Dealership.Api.Layer.Controllers.Identity
{
    [Route("identity/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        public void AccessDenied()
        {
        }

        public void Login()
        {
        }

        public async Task<ApiResult<User>> Register(User user)
        {
            try
            {
            }
            catch
            {
            }
        }
    }
}