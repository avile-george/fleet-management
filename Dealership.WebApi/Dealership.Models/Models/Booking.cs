﻿using System;

namespace Dealership.Models
{
    public class Booking
    {
        public int Id { get; set; }

        public int VehicleId { get; set; }

        public string UserId { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsActive { get; set; }

        public Vehicle Vehicle { get; set; }

        public User User { get; set; }

        public double Price { get; set; }
    }
}