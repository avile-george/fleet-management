﻿namespace Dealership.Models
{
    public class ApiResult<T>
    {
        public ApiResult()
        {
        }

        public ApiResult(T result)
        {
            Result = result;
        }

        public string Error { get; set; }
        public string Warning { get; set; }
        public string Information { get; set; }
        public T Result { get; set; }
    }
}