﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Dealership.Models
{
    public class User
    {
        public string Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IdNumber { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public bool IsActive { get; set; }

        public List<UserModel> FavouriteModels { get; set; }

        public List<IdentityRole> Roles { get; set; }
    }
}