﻿namespace Dealership.Models
{
    public class UserModel
    {
        public int UserId { get; set; }

        public User User { get; set; }

        public int ModelId { get; set; }

        public VehicleModel Model { get; set; }
    }
}