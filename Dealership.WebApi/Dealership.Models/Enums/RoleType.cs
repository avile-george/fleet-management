﻿namespace Dealership.Models
{
    public enum RoleType
    {
        // Explicit enum values just to be safe because they map to db values
        Admin = 0,

        User = 1
    }
}