﻿using System;
using AutoMapper;
using Dealership.Models;
using db = Dealership.Data.Entities;

namespace Dealership.Data.Helpers
{
    public class MapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<db.Booking, Booking>();
                cfg.CreateMap<Booking, db.Booking>();

                cfg.CreateMap<db.VehicleMake, VehicleMake>();
                cfg.CreateMap<VehicleMake, db.VehicleMake>();

                cfg.CreateMap<db.VehicleModel, VehicleModel>().
                ForMember(dest => dest.ImageBase64string, src => src.MapFrom(x => Convert.ToBase64String(x.Image)));
                cfg.CreateMap<VehicleModel, db.VehicleModel>().
                ForMember(dest => dest.Image, src => src.MapFrom(x => Convert.FromBase64String(x.ImageBase64string)));

                cfg.CreateMap<db.Vehicle, Vehicle>();
                cfg.CreateMap<Vehicle, db.Vehicle>();

                cfg.CreateMap<db.User, User>();
                cfg.CreateMap<User, db.User>();

                cfg.CreateMap<db.UserModel, UserModel>();
                cfg.CreateMap<UserModel, db.UserModel>();
            });
        }
    }
}