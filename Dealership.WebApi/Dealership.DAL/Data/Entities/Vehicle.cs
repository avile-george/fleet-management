﻿namespace Dealership.Data.Entities
{
    public class Vehicle
    {
        public int Id { get; set; }

        public string RegNumber { get; set; }

        public int ModelId { get; set; }

        public string Colour { get; set; }

        public int Year { get; set; }

        public VehicleModel Model { get; set; }
        public bool IsActive { get; set; }
    }
}