﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Dealership.Data.Entities
{
    public class UserModel
    {
        [Key, Column(Order = 0)]
        public int UserId { get; set; }

        public User User { get; set; }

        [Key, Column(Order = 1)]
        public int ModelId { get; set; }

        public VehicleModel Model { get; set; }
    }
}