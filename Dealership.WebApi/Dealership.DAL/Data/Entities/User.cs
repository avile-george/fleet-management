﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Dealership.Data.Entities
{
    public class User : IdentityUser
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string IdNumber { get; set; }

        public string RoleId { get; set; }

        public bool IsActive { get; set; }

        public List<IdentityRole> Roles { get; set; }

        public ICollection<UserModel> FavouriteModels { get; set; }
    }
}