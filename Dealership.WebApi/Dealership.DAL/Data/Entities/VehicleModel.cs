﻿namespace Dealership.Data.Entities
{
    public class VehicleModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int ParentMakeId { get; set; }

        public VehicleMake ParentMake { get; set; }

        public byte[] Image { get; set; }

        public double PricePerDay { get; set; }

        public int NumberOfSeats { get; set; }
    }
}