﻿using Dealership.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Dealership.Data
{
    public class DealershipContext : IdentityDbContext<User>
    {
        public DealershipContext()
        {
        }

        public DealershipContext(DbContextOptions<DealershipContext> options)
             : base(options)
        {
        }

        public DbSet<Booking> Bookings { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<VehicleModel> Models { get; set; }

        public DbSet<VehicleMake> Makes { get; set; }

        public DbSet<UserModel> UserModels { get; set; }

        //public DbSet<Role> Roles { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // modelBuilder.Entity<User>().ToTable("User");

            modelBuilder.Entity<UserModel>()
                .HasKey(c => new { c.UserId, c.ModelId });
        }
    }
}