﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Dealership.Models;

namespace Dealership.Data.Services
{
    public interface IDataService
    {
        Task<Booking> AddNewBookingAsync(Booking booking);

        Task<Vehicle> DeleteVehicleAsync(int Id);

        Task<IList<Booking>> GetBookingsAsync(bool includePassedBookings = false);

        Task<Vehicle> GetVehicleAsync(int Id);

        Task<List<VehicleModel>> GetVehicleMakeModelsAsync(int makeId);

        Task<List<VehicleMake>> GetVehicleMakesAsync();

        Task<IList<Vehicle>> GetVehiclesAsync();

        Task<Vehicle> SaveNewVehicleAsync(Vehicle vehicle);

        Task<Booking> UpdateBookingAsync(Booking booking);

        Task<Vehicle> UpdateVehicleAsync(Vehicle vehicle);

        Task<User> GetUserAsync(string username, string password);

        Task<User> GetUserAsync(int Id);
    }
}