﻿using System.Threading.Tasks;
using Dealership.Models;

namespace Dealership.Data.Services
{
    public interface IIdentityService
    {
        Task<User> CreateUserAsync(User user);
        Task<User> GetUserAsync(string id);
    }
}