﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dealership.Models;
using Microsoft.EntityFrameworkCore;
using db = Dealership.Data.Entities;

namespace Dealership.Data.Services
{
    public class DataService : IDataService
    {
        private readonly DealershipContext _context;

        public DataService(DealershipContext context)
        {
            _context = context;
        }

        public async Task<List<VehicleModel>> GetVehicleMakeModelsAsync(int makeId)
        {
            List<db.VehicleModel> dbModels = await _context.Models.Where(m => m.ParentMakeId == makeId).Include(m => m.ParentMake).ToListAsync();
            return Mapper.Map<List<VehicleModel>>(dbModels);
        }

        public async Task<List<VehicleMake>> GetVehicleMakesAsync()
        {
            List<db.VehicleMake> dbMakes = await _context.Makes.ToListAsync();
            return Mapper.Map<List<VehicleMake>>(dbMakes);
        }

        public async Task<IList<Vehicle>> GetVehiclesAsync()
        {
            List<db.Vehicle> dbVehicles = await _context.Vehicles.Include(v => v.Model).Include(m => m.Model.ParentMake).ToListAsync();

            return Mapper.Map<List<Vehicle>>(dbVehicles);
        }

        public async Task<IList<Booking>> GetBookingsAsync(bool includePassedBookings = false)
        {
            List<db.Booking> dbBookings = await _context.Bookings.Where(b => includePassedBookings || b.EndDate > DateTime.Now)
                .Include(b => b.Vehicle)
                .Include(b => b.Vehicle.Model)
                .Include(b => b.Vehicle.Model.ParentMake)
                .Include(b => b.User)
                .ToListAsync();
            return Mapper.Map<List<Booking>>(dbBookings);
        }

        public async Task<Booking> GetBookingAsync(int Id, bool includePassedBookings = true)
        {
            db.Booking dbBooking = await _context.Bookings.Where(b => includePassedBookings || b.EndDate > DateTime.Now)
                .Include(b => b.Vehicle)
                .Include(b => b.Vehicle.Model)
                .Include(b => b.Vehicle.Model.ParentMake)
                .Include(b => b.User)
                .Where(b => b.Id == Id)
                .FirstAsync();
            return Mapper.Map<Booking>(dbBooking);
        }

        public async Task<Booking> UpdateBookingAsync(Booking booking)
        {
            db.Booking dbBooking = await _context.Bookings.Where(b => b.Id == booking.Id).Include(b => b.Vehicle).FirstOrDefaultAsync();

            dbBooking.VehicleId = booking.VehicleId;

            if (booking.StartDate > DateTime.Now)
            {
                dbBooking.StartDate = booking.StartDate;
            }

            if (booking.EndDate > DateTime.Now)
            {
                dbBooking.EndDate = booking.EndDate;
            }

            dbBooking.IsActive = booking.IsActive;

            await _context.SaveChangesAsync();

            return Mapper.Map<Booking>(dbBooking);
        }

        public async Task<Booking> AddNewBookingAsync(Booking booking)
        {
            // Because of lack of validation, booking can be linked to a model that has no vehicles
            booking.VehicleId = Mapper.Map<Vehicle>(await _context.Vehicles.FirstAsync(v => v.ModelId == booking.Vehicle.ModelId)).Id;
            booking.Vehicle = null;
            db.Booking dbBooking = Mapper.Map<db.Booking>(booking);
            await _context.Bookings.AddAsync(dbBooking);
            await _context.SaveChangesAsync();
            return await GetBookingAsync(dbBooking.Id);
        }

        public async Task<Vehicle> GetVehicleAsync(int Id)
        {
            db.Vehicle dbVehicle = await _context.Vehicles
                .Include(v => v.Model)
                .Include(v => v.Model.ParentMake)
                .Where(v => v.Id == Id)
                .FirstOrDefaultAsync();

            return Mapper.Map<Vehicle>(dbVehicle);
        }

        public async Task<Vehicle> SaveNewVehicleAsync(Vehicle vehicle)
        {
            vehicle.ModelId = vehicle.Model.Id;
            vehicle.Model = null;
            db.Vehicle dbVehicle = Mapper.Map<db.Vehicle>(vehicle);
            await _context.Vehicles.AddAsync(dbVehicle);
            await _context.SaveChangesAsync();
            return await GetVehicleAsync(dbVehicle.Id);
        }

        public async Task<Vehicle> UpdateVehicleAsync(Vehicle vehicle)
        {
            db.Vehicle dbVehicle = await _context.Vehicles.Where(v => v.RegNumber == vehicle.RegNumber).FirstOrDefaultAsync();

            dbVehicle.Year = vehicle.Year;
            dbVehicle.Colour = vehicle.Colour;

            await _context.SaveChangesAsync();

            return Mapper.Map<Vehicle>(dbVehicle);
        }

        public async Task<Vehicle> DeleteVehicleAsync(int Id)
        {
            db.Vehicle dbVehicle = await _context.Vehicles.Where(v => v.Id == Id).FirstOrDefaultAsync();
            List<db.Booking> vehicleBookings = _context.Bookings.Where(b => b.VehicleId == dbVehicle.Id).ToList();

            if (vehicleBookings.Any())
            {
                dbVehicle.IsActive = false;
            }
            else
            {
                _context.Vehicles.Remove(dbVehicle);
            }

            await _context.SaveChangesAsync();

            return Mapper.Map<Vehicle>(dbVehicle);
        }

        public async Task<User> GetUserAsync(string username, string password)
        {
            db.User dbUser = await _context.Users
                .Where(u => u.Email == username && u.PasswordHash == password && u.IsActive)
                .Include(u => u.FavouriteModels)
                .FirstOrDefaultAsync();

            return Mapper.Map<User>(dbUser);
        }
    }
}