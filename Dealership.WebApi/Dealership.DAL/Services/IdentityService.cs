﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Dealership.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using db = Dealership.Data.Entities;

namespace Dealership.Data.Services
{

    public class ApplicationRoleManager : RoleManager<IdentityRole>
    {
        public ApplicationRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
        {
        }

        public static ApplicationRoleManager Create(IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context)
        {
            var appRoleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>()));

            return appRoleManager;
        }
    }

    public class IdentityService : IIdentityService
    {
        private readonly UserManager<db.User> _userManager;
        private readonly DealershipContext _context;

        public IdentityService(UserManager<db.User> userManager, DealershipContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<User> CreateUserAsync(User user)
        {
            if (await _userManager.FindByEmailAsync(user.Email) == null)
            {
                db.User dbUser = Mapper.Map<db.User>(user);
                await _userManager.CreateAsync(dbUser, $"MyDealership{DateTime.Now.Year}!");

                return await GetUserAsync(dbUser.Id);
            }

            throw new Exception("Email must be unique");
        }

        public async Task<User> GetUserAsync(string id)
        {
            db.User dbUser = await _context.Users
              .Where(u => u.Id == id.ToString() && u.IsActive)
              .Include(u => u.FavouriteModels)
              .FirstOrDefaultAsync();

            return Mapper.Map<User>(dbUser);
        }

        public async Task<IdentityRole> GetRoleByNameAsync(string roleName)
        {
            var role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);

            return Mapper.Map<IdentityRole>(role);
        }

        public async Task<IdentityUserRole> CreateUser
    }
}