import { Component, OnInit, Inject } from '@angular/core';
import { Vehicle } from 'app/shared/interfaces/vehicle.interface';
import { Make } from 'app/shared/interfaces/make.interface';
import { Model } from 'app/shared/interfaces/model.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from 'app/shared/services/user.service';
import { ApiService } from 'app/shared/services/api.service';

@Component({
  selector: 'app-vehicle-dialog',
  templateUrl: './vehicle-dialog.component.html',
  styleUrls: ['./vehicle-dialog.component.scss']
})
export class VehicleDialogComponent implements OnInit {

  isEdit = false;
  makeList: Make[] = [];
  modelList: Model[] = [];


  constructor(
    public dialogRef: MatDialogRef<VehicleDialogComponent>,
    private userServ: UserService,
    private ApiServ: ApiService,

    @Inject(MAT_DIALOG_DATA) public vehicle: Vehicle) {
    this.isEdit = vehicle ? true : false;
  }

  ngOnInit() {
    this.GetAllMakes();

    if (!this.isEdit) {
      this.GetEmptyVehicle();
    } else {
      this.GetModels();
    }

  }

  GetEmptyVehicle() {
    this.vehicle = {
      model: {
        parentMake: {
        }
      },
      regNumber: '',
      isActive: true
    }
  }

  GetModels() {
    this.ApiServ.Vehicle.GetAllModelsByMake(this.vehicle.model.parentMake.id).subscribe(models => {
      this.modelList = models;
    })
  }

  GetAllMakes() {
    this.ApiServ.Vehicle.GetAllMakes().subscribe(makes => {
      this.makeList = makes;
    })
  }

  SaveVehicle() {
    this.vehicle.modelId = this.vehicle.model.id;
    if (this.isEdit) {
      this.ApiServ.Vehicle.UpdateVehicle(this.vehicle).subscribe(_vehicle => {
        this.dialogRef.close(_vehicle);
      })
    } else {
      this.ApiServ.Vehicle.CreateVehicle(this.vehicle).subscribe(_vehicle => {
        this.dialogRef.close(_vehicle);
      })
    }
  }
}
