import { Component, OnInit, Inject } from '@angular/core';
import { Booking } from 'app/shared/interfaces/booking.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Model } from 'app/shared/interfaces/model.interface';
import { UserService } from 'app/shared/services/user.service';
import * as moment from 'moment';
import { ApiService } from 'app/shared/services/api.service';
import { Make } from 'app/shared/interfaces/make.interface';

@Component({
  selector: 'app-booking-dialog',
  templateUrl: './booking-dialog.component.html',
  styleUrls: ['./booking-dialog.component.scss']
})
export class BookingDialogComponent implements OnInit {

  isEdit = false;
  makeList: Make[] = [];
  modelList: Model[] = [];

  constructor(
    public dialogRef: MatDialogRef<BookingDialogComponent>,
    private userServ: UserService,
    private ApiServ: ApiService,
    @Inject(MAT_DIALOG_DATA) public booking: Booking) {

    this.isEdit = booking && booking.id ? true : false;
    this.GetAllMakes();

    if (!this.isEdit) {
      this.GetEmptyBooking();
    } else {
      this.booking.model = this.booking.vehicle.model;
      this.booking.modelId = this.booking.model.id;
      this.GetModels();
    }
  }

  ngOnInit() {
  }

  GetEmptyBooking() {
    const booking: Booking = {
      model: {
        parentMake: {}
      },
      vehicle: {
        model: {
          parentMake: {
          }
        },
        regNumber: '',
        isActive: true
      },
      isActive: true,
      startDate: null,
      endDate: null
    };

    if (this.booking && this.booking.model) {
      booking.model = booking.vehicle.model = this.booking.model;
      booking.modelId = booking.vehicle.modelId = this.booking.modelId;
      this.CalculatePrice();
      this.GetModels();
    }

    this.booking = booking;
  }

  myFilterStartDate = (d: Date): boolean => {
    return moment().isSameOrBefore(d);
  }

  CalculatePrice() {
    let days = Math.abs(moment(this.booking.startDate).dayOfYear() - moment(this.booking.endDate).dayOfYear());
    // let days = moment(this.booking.endDate).diff(moment(this.booking.startDate))
    days = isNaN(days) || days < 1 ? 1 : days;
    const price = +(this.booking.model.pricePerDay * days).toFixed(2);
    // tslint:disable-next-line:radix
    this.booking.price = price;
  }

  ModelSelected() {
    this.booking.model = this.modelList.find(l => l.id === this.booking.modelId);
    this.CalculatePrice();
  }

  myFilterEndDate = (d: Date): boolean => {
    return moment().isSameOrBefore(d);
  }


  GetModels() {
    this.ApiServ.Vehicle.GetAllModelsByMake(this.booking.model.parentMake.id).subscribe(models => {
      this.modelList = models;
    })
  }

  GetAllMakes() {
    this.ApiServ.Vehicle.GetAllMakes().subscribe(makes => {
      this.makeList = makes;
    })
  }

  SaveBooking() {
    this.booking.modelId = this.booking.model.id;
    //  this.booking.price = this.CalculatePrice();

    if (this.isEdit) {
      this.ApiServ.Booking.UpdateBooking(this.booking).subscribe(_booking => {
        this.dialogRef.close(_booking);
      })
    } else {
      this.booking.vehicle.model = this.booking.model;
      this.booking.vehicle.modelId = this.booking.modelId;

      this.ApiServ.Booking.CreateBooking(this.booking).subscribe(_booking => {
        this.dialogRef.close(this.booking);
      })
    }
  }
}
