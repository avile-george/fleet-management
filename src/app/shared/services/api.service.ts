import { Injectable } from '@angular/core';
import { Headers, Http, ResponseContentType } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import { environment } from 'environments/environment';
import { User } from '../interfaces/user.interface';
import { Booking } from '../interfaces/booking.interface';
import { Vehicle } from '../interfaces/vehicle.interface';
import { Make } from '../interfaces/make.interface';
import { Model } from '../interfaces/model.interface';
import { UserModel } from '../interfaces/favourite.interface';

@Injectable()
export class ApiService {
    ContractTerm = 60;
    User: UserController;
    Booking: BookingController;
    Vehicle: VehicleController;
    TestData: Model[];

    constructor(private http: Http,
        private router: Router) {
        const headers = new Headers({ 'Accept': 'application/json' });
        headers.append('Authorization', 'Bearer ' + localStorage.getItem('access_token'));
        headers.append('Content-Type', 'application/json');

        const config = new ControllerConfig(http, headers, environment.apiEndpoint, router);

        this.User = new UserController(config, 'User');
        this.Booking = new BookingController(config, 'Booking');
        this.Vehicle = new VehicleController(config, 'Vehicle');
        this.TestData = [
          {
                id: 2,
                name: 'A4',
                numberOfSeats: 5,
                pricePerDay: 300.15,
                // tslint:disable-next-line:max-line-length
                imageBase64string: 'iVBORw0KGgoAAAANSUhEUgAAAgIAAAEcCAYAAAC1cdy6AAAMGGlDQ1BJQ0MgUHJvZmlsZQAASImVlwdUU0kXx+eVFEJCC0RASuhNegfpXRCQDjZCEiCUEBKCih1dVHDtYsGKroIouBZAFhWxK4uAvW5QUVHWxYINlW+SALruV853z5n3frlz585/JjPvzACg7MQSCHJQFQBy+QXCmBB/ZlJyCpMkAQjAgQIYCzxZbJHALzo6AkAbef/d3t2A0dCuWktz/bP+v5oqhytiA4BEQ07jiNi5kI8AgGuzBcICAAgd0G80o0Ag5beQ1YVQIABEspQz5Kwj5TQ528li4mICIAcCQKayWMIMAJSk+ZmF7AyYR0kA2Y7P4fEh74Dszc5kcSBLII/Lzc2DrEyFbJ72XZ6Mv+VMG83JYmWMsnwsMiMH8kSCHNas/3M6/rfl5ohH+jCEhZopDI2RjhnOW1V2XriUoXakmZ8WGQVZDfIFHkcWL+U7meLQ+OH4PrYoAM4ZYACAAg4rMBwynEuUIc6O9xtmB5ZQ1hbGo5G8grC4YU4T5sUM50cL+TmREcN5lmZyw0Z4G1cUFDsSk84LDoMMVxp6pCgzLlGuEz1TyEuIhKwEuUOUHRs+3PZBUWZA5EiMUBwj1WwM+W26MDhGHoNp5opGxoXZsFmyvjQh+xZkxoXK22JJXFFSxIgGDjcwSK4B43D58cPaMLi6/GOG25YIcqKH47Ft3JyQGPk8YwdFhbEjbbsK4AKTzwP2MIs1IVquH3snKIiOk2vDcRABAkAgYAIxLGkgD2QBXntfQx/8Ja8JBiwgBBmAC6yHPSMtEmU1fPiMBUXgT0hcIBpt5y+r5YJC6P8y6pU/rUG6rLZQ1iIbPIGci2vj3rgnHgGfvrA44G64+0g7pvJIr8QgYiAxlBhMtBjVwYaqc2ARAt6/8YXDNxeOTqqFPzKGb/kITwidhIeE6wQJ4TZIAI9lWYajpvOKhT8oZ4KJQAKzBQ+PLg3m7B2JwU2hamfcH/eC+qF2nIFrA2vcCY7ED/eBY3OG3u8Vike1fZvLH/uTqv5+PMN+JUsl52EVaaP/TMBo1I9ZAr6bIw58h/8YiS3FDmPnsVPYRawZawBM7CTWiLVhx6U8uhIey1bCSG8xMm3ZMA9vJMauxq7X7vM/emcNKxDK/m9QwJ1ZIN0QAXmCWUJeRmYB0w9+kbnMMD7bZhzTwc7eBQDp913++XjDkH23Ecalb778FgDcS6Ez45uPZQTAsScA0N998xm9httrFQDHO9hiYaHch0sfBEABynBnaAE9YATM4ZgcgAvwBL4gCEwAUSAOJINpcNYzQS5UPQPMAQtBCSgDq8B6sBlsB7tAFTgADoEG0AxOgXPgMugA18FduDZ6wAvQD96BQQRBSAgNoSNaiD5iglghDogb4o0EIRFIDJKMpCIZCB8RI3OQRUgZsgbZjOxEqpFfkWPIKeQi0oncRrqRXuQ18gnFUCqqjuqipqgt6ob6oeFoHDoVzUDz0SJ0MboC3YhWovvRevQUehm9jkrQF+gABjBFjIEZYNaYGxaARWEpWDomxOZhpVg5VonVYk3wv76KSbA+7CNOxOk4E7eG6zMUj8fZeD4+D1+Ob8ar8Hr8DH4V78b78a8EGkGHYEXwIIQRkggZhBmEEkI5YQ/hKOEs3Ds9hHdEIpFBNCO6wr2ZTMwiziYuJ24l1hFbiJ3ER8QBEomkRbIieZGiSCxSAamEtIm0n3SS1EXqIX0gK5L1yQ7kYHIKmU8uJpeT95FPkLvIT8mDCioKJgoeClEKHIVZCisVdis0KVxR6FEYpKhSzChelDhKFmUhZSOllnKWco/yRlFR0VDRXXGSIk9xgeJGxYOKFxS7FT9S1aiW1ADqFKqYuoK6l9pCvU19Q6PRTGm+tBRaAW0FrZp2mvaA9kGJrmSjFKbEUZqvVKFUr9Sl9FJZQdlE2U95mnKRcrnyYeUryn0qCiqmKgEqLJV5KhUqx1Ruqgyo0lXtVaNUc1WXq+5Tvaj6TI2kZqoWpMZRW6y2S+202iM6RjeiB9DZ9EX03fSz9B51orqZeph6lnqZ+gH1dvV+DTUNJ40EjZkaFRrHNSQMjGHKCGPkMFYyDjFuMD6N0R3jN4Y7ZtmY2jFdY95rjtX01eRqlmrWaV7X/KTF1ArSytZardWgdV8b17bUnqQ9Q3ub9lntvrHqYz3HsseWjj009o4OqmOpE6MzW2eXTpvOgK6eboiuQHeT7mndPj2Gnq9elt46vRN6vfp0fW99nv46/ZP6z5kaTD9mDnMj8wyz30DHINRAbLDToN1g0NDMMN6w2LDO8L4RxcjNKN1onVGrUb+xvvFE4znGNcZ3TBRM3EwyTTaYnDd5b2pmmmi6xLTB9JmZplmYWZFZjdk9c5q5j3m+eaX5NQuihZtFtsVWiw5L1NLZMtOywvKKFWrlYsWz2mrVOY4wzn0cf1zluJvWVGs/60LrGutuG4ZNhE2xTYPNS1tj2xTb1bbnbb/aOdvl2O22u2uvZj/Bvti+yf61g6UD26HC4ZojzTHYcb5jo+MrJysnrtM2p1vOdOeJzkucW52/uLi6CF1qXXpdjV1TXbe43nRTd4t2W+52wZ3g7u8+373Z/aOHi0eBxyGPvzytPbM993k+G282njt+9/hHXoZeLK+dXhJvpneq9w5viY+BD8un0uehr5Evx3eP71M/C78sv/1+L/3t/IX+R/3fB3gEzA1oCcQCQwJLA9uD1ILigzYHPQg2DM4IrgnuD3EOmR3SEkoIDQ9dHXozTDeMHVYd1j/BdcLcCWfCqeGx4ZvDH0ZYRggjmiaiEydMXDvxXqRJJD+yIQpEhUWtjbofbRadH/3bJOKk6EkVk57E2MfMiTkfS4+dHrsv9l2cf9zKuLvx5vHi+NYE5YQpCdUJ7xMDE9ckSpJsk+YmXU7WTuYlN6aQUhJS9qQMTA6avH5yzxTnKSVTbkw1mzpz6sVp2tNyph2frjydNf1wKiE1MXVf6mdWFKuSNZAWlrYlrZ8dwN7AfsHx5azj9HK9uGu4T9O90tekP8vwylib0Zvpk1me2ccL4G3mvcoKzdqe9T47Kntv9lBOYk5dLjk3NfcYX42fzT+Tp5c3M69TYCUoEUjyPfLX5/cLw4V7RIhoqqixQB0eddrE5uKfxN2F3oUVhR9mJMw4PFN1Jn9m2yzLWctmPS0KLvplNj6bPbt1jsGchXO65/rN3TkPmZc2r3W+0fzF83sWhCyoWkhZmL3w92K74jXFbxclLmparLt4weJHP4X8VFOiVCIsubnEc8n2pfhS3tL2ZY7LNi37WsopvVRmV1Ze9nk5e/mln+1/3vjz0Ir0Fe0rXVZuW0VcxV91Y7XP6qo1qmuK1jxaO3Ft/TrmutJ1b9dPX3+x3Kl8+wbKBvEGycaIjY2bjDet2vR5c+bm6xX+FXVbdLYs2/J+K2dr1zbfbbXbdbeXbf+0g7fj1s6QnfWVppXlu4i7Cnc92Z2w+/wvbr9U79HeU7bny17+XklVTNWZatfq6n06+1bWoDXimt79U/Z3HAg80FhrXbuzjlFXdhAcFB98/mvqrzcOhR9qPex2uPaIyZEtR+lHS+uR+ln1/Q2ZDZLG5MbOYxOOtTZ5Nh39zea3vc0GzRXHNY6vPEE5sfjE0MmikwMtgpa+UxmnHrVOb717Oun0tTOTzrSfDT974VzwudPn/c6fvOB1ofmix8Vjl9wuNVx2uVzf5tx29Hfn34+2u7TXX3G90tjh3tHUOb7zRJdP16mrgVfPXQu7dvl65PXOG/E3bt2cclNyi3Pr2e2c26/uFN4ZvLvgHuFe6X2V++UPdB5U/mHxR53ERXK8O7C77WHsw7uP2I9ePBY9/tyz+AntSflT/afVzxyeNfcG93Y8n/y854XgxWBfyZ+qf255af7yyF++f7X1J/X3vBK+Gnq9/I3Wm71vnd62DkQPPHiX+27wfekHrQ9VH90+nv+U+Onp4IzPpM8bv1h8afoa/vXeUO7QkIAlZMmOAhgsaHo6AK/3AkBLhmcHeI+jKMnvXzJD5HdGGYH/xPI7mszgyWWvLwDxCwCIgGeUbbCYQKbCt/T4HecLUEfH0TJsonRHB3kuKrzFED4MDb3RBYDUBMAX4dDQ4NahoS+7odjbALTky+99UiPCM/4OWyl19LwEP9q/ANYzbXdPwU0PAAAACXBIWXMAABYlAAAWJQFJUiTwAAABnWlUWHRYTUw6Y29tLmFkb2JlLnhtcAAAAAAAPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iWE1QIENvcmUgNS40LjAiPgogICA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPgogICAgICA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIgogICAgICAgICAgICB4bWxuczpleGlmPSJodHRwOi8vbnMuYWRvYmUuY29tL2V4aWYvMS4wLyI+CiAgICAgICAgIDxleGlmOlBpeGVsWERpbWVuc2lvbj41MTQ8L2V4aWY6UGl4ZWxYRGltZW5zaW9uPgogICAgICAgICA8ZXhpZjpQaXhlbFlEaW1lbnNpb24+Mjg0PC9leGlmOlBpeGVsWURpbWVuc2lvbj4KICAgICAgPC9yZGY6RGVzY3JpcHRpb24+CiAgIDwvcmRmOlJERj4KPC94OnhtcG1ldGE+ClcoViAAAAAcaURPVAAAAAIAAAAAAAAAjgAAACgAAACOAAAAjgAAGCGultlCAAAX7UlEQVR4Aeydj08cR5qGe2VHC1qsgAxnkxhfYhucxVlwILKjeLWRdv/uO11Ol9UlOrwxuyYxdrDXOMEJRBBBZKI1ytXbuHHP0N3TPdPV01X1lISYHz01VU/VdL311Vdf/eZXkyISBCAAAQhAAAJBEvgNQiDIdqfSEIAABCAAgZgAQoCOAAEIQAACEAiYAEIg4Man6hCAAAQgAAGEAH0AAhCAAAQgEDABhEDAjU/VIQABCEAAAggB+gAEIAABCEAgYAIIgYAbn6pDAAIQgAAEEAL0AQhAAAIQgEDABBACATc+VYcABCAAAQggBOgDEIAABCAAgYAJIAQCbnyqDgEIQAACEEAI0AcgAAEIQAACARNACATc+FQdAhCAAAQggBCgD0AAAhCAAAQCJoAQCLjxqToEIAABCEAAIUAfgAAEIAABCARMACEQcONTdQhAAAIQgABCgD4AAQhAAAIQCJgAQiDgxqfqEIAABCAAAYQAfQACEIAABCAQMAGEQMCNT9UhAAEIQAACCAH6AAQgAAEIQCBgAgiBgBufqkMAAhCAAAQQAvQBCEAAAhCAQMAEEAIBNz5VhwAEIAABCCAE6AMQgAAEIACBgAkgBAJufKoOAQhAAAIQQAjQByAAAQhAAAIBE0AIBNz4VB0CEIAABCCAEKAPQAACEIAABAImgBAIuPGpOgQgAAEIQAAhQB+AAASsEtg/OIiOXh7F37G/fxC9fPny5Pv+ZR4fmNeykj6XvjbrmmG8NjY2Fr1x9mzhV4+dy77mrPncOfNedxob+12k90gQGAYBhMAwqPOdEPCAwOHhYXR4+Ev04oX+H0Yv9Nw8Vtrd2/OghsOtwsjISDRq/pKUJSImJsaTt6ORkd+av9fXn7zBAwj0IIAQ6AGItyEQMgHNyA8Ofo40k9cMXQN9POCbQZ/UXgKxaDCWiySlBUP6MZaIhFDY/xECYbc/tYfACQHN6vfNoC9T/e7uHgP+CZkwHqSXPLS0IWuEljEQC/63P0LA/zamhhDIJKAZ/t7uT/GgH8/2meVncuLFKPZfOGcsDIlAGJ94M9Jzkh8EEAJ+tCO1gEBPAnt7rwZ9zfjNGn4bHfF6VoILWkNAyw8T4+ORlhoQBq1plr4KghDoCxsfgkC7CSRr+zLxx38477W7wTwoXSIMpqYmo6mp8+yCcKhNEQIONRZFhUAegWR9Pxn4D4zZnwSBYRKYmpQgQBQMsw3KfjdCoCwproNAiwho4N/V+r6Z6Wvw13MSBNpKAFHQ1pY5LhdCoN3tQ+kgEBNIO/axvk+ncJWAlg9kJbjy7r8T86BFjYgQaFFjUBQIJAQSxz7N9iUCcOxLyPDfFwJyNJy5fCmamjzvS5WcrQdCwNmmo+A+Edje+fFk/z5R+XxqWerSi4CiIV6euRRNT1/AwbAXLEvvIwQsgSVbCBQR0Jr+9vaP8fr+9s5O0aWteK8ofG06Ul1eYbsj3eVdV8frVSwosrQoamJRknXGh5Ruw6TNFDQoLyxxmmMSUVIckh0pdTNRH5EgeNcsG5CaJYAQaJY33xYwAc36Zerf3t5pzLnvzJkzpw65SQaBpCkUGEY34SSdfcMcjEOwmARH7v9ERKx99aCx9swqTK8Bvru9s/Ko+prqnixb6X+dYknC5Mq778QWgqrl4vr+CCAE+uPGpyDQk0Ads/7x8Tc7vkchX9ODdvcgPjqaP8PryIgntRCQCNjael5LXmUyUbhfDexqdw2YNgb5MuXIukaCINnFUocwQBBkUbbzGkLADldyDZSAZkoy+T/dfGYO6yk2OacRaVY3Y8yi/2Y8qvNMtenreTx8Ak2JAA3+09MXnesbEkjfmb9BRYHOQJifv46VymKXRwhYhEvW4RCQ2V8m/yqzQw3+2kqlmzymeLf6yvrDR9Hm5rdWCz198ULsVe9635CvwebTZ9EP5vdxdHTUNzMJZW07TFvE+s6MD3YQQAh04OAJBMoTkOl/a+v7eNZTNqAPg395vm29UmJP1gBbaWbm7dhpzjfL0LG1bCfaePzE+FT80hc+MZmbvRoL6L4y4EOZBBACmVh4EQL5BDT4azAou82PwT+fpWvv2BQB8geZm7sWhHVI/gQSBP0uGyhSoZYLsA7U8wtCCNTDkVw8J6DZjEzBWvvX415J3vpvvXURs38vUA69b0sEqK9cNwJAS0ShJQmCta++7stCIBEg60CI3OruJwiBuomSn1cEZPJ/agSABoEyAkCzurfMDZ2bk1fdII7uePfuvVJ9oErN1V8WF94PfmYr68BT40fQjw+BfmsSBFgHqvS8zmsRAp08eAaBmIAEwMbjf5Zy/pPpXzcjCQDf1nXpDpE1EaDAOdovTzomoN/cg/VH0Y5xvK2a2FlQlVjn9QiBTh48C5yATJWPjQAos/4vr+7jY1YnA6fmb/Xl8V63JUBLATfm38PhLafb9LtcIIuAhJWcLUnVCCAEqvHiak8JlHUA1Oxf25g0+8cU6WlneFUtLQV9/sVKrVEDJQKWl28G4RA4aO/QcoFEedWEI2FVYlGEEKjOjE94ROB49tE7RKzWchUHXRYAkv8EJAJWjE9AlaBQvagoMJD8AVg+6kXq9fuyyKytfW3a4efXL5Z4xFJBCUipSxACKRg8DIeAbjAP178pXALQ7E2R/q5ceYebdzhdI3YItCEClpduYkXqsx/1E8BJFrulpUWsLyWYIwRKQOISfwiUcQJMnP9kAcD870/bl62JfALK+IiUzU+WAERAWVr51/XrOzD/++vs4snHGr+DEOgBiLf9ICBTb684AGz986OtB6lF3ecHIAIGaY3Tn9XveN3sLNh6/v3pNwtemZu9hhNhAR+EQAEc3vKDgBwBj8OaHmZWSAJA3sZtOskts6C8aJVA3SIAx0B7zaVzPe4b34EqcQe0xVfWAdJpAgiB00x4xRMCMiWuP/wm1+ELAeBJQ9dQjX7WoIu+FhFQRKee92QdkBioEndgYnw8Wli4wZJfVxMgBLqA8NR9AvIDWFt7kLvOiwBwv43rrIGN0MFLHyxiYaqzkQryUtjvjY0npa0D2lGwbJwI8f95DRUh8JoFjzwgoH3HWgbISgiALCphv2ZDBBAxsPk+JfF/b/UfpbcZIgY62wgh0MmDZ44SKIoHoF0AchYiBoCjjWup2DZEwOTk+ThWgKUik20PAlWCELG98DVMhMBrFjxykIDWCXUmwKYxD3YnrdNevnwpDgSEGbCbTtjPbYQOZodAO/pUlSBEiIHjNkMItKPvUoo+CBRZAXQOAIGA+oAawEdsiACcA9vVcY4nCE/iLcO9SoYYIMRwrz7C+y0kEO8lNrsBZNrtTpqVaRmArYDdZHguAjZEgPIlaI0otC9psiDfgTLbDENuQywC7eu7lKiAgPYPa7+3xEA6aUZ2fe4aEcTSUHjcQUAOZTpEqLvvdFzUxxNZn+bNaYKkdhKQ+FtZ+RIxUNA8CIECOLzVHgK6eWtL4PbOzqlCyUFLx7riB3AKDS+8IqD+U/f5AcoavwA3uhhioLidEALFfHi3BQSKrACc696CBmp5EWyJAFX71q1lDrVpefsnxasiBkKLQogQSHoJ/1tHoMgKoJgAEgEc6dq6ZmtVgWyKgNnZq/GOlFZVmMIUElDwoYfGv6hMCkkMIATK9AiuaZyAnHxW/37/1HqufAG0G0AnA5IgUETApgggXkAR+Xa/99lf/zc6PPylVCElBuaM4PN92REhUKo7cFGTBPKiA2o9dnHhfawATTaGo99lUwQoQNXtWx96Pzg42vQ9i101kFQIUQgRAj27DRc0RUA379XV+5lnBBC2talWcP97bIoA0cEvwO0+Imvj3b/dq1QJ38UAQqBSd+BiWwSKlgJkBSAugC3yfuVrWwTgF+B+f5HzsZYdqyaJgUVzcqGPfkkIgaq9getrJ5C3FCCHQIkA39fnagcaaIa2RYD64/LSzUDp+lNtWR2ztiGXqaGvUQgRAmVan2usECg6LpilACvIvc3UtgiQk+of73yEKHW8BykYWVZE0irV8lEMIASq9ACurY2AzHN5EQJZCqgNcxAZ2RYBgrj0wSLLUw73JvWRvIBk/VRLYmDhDze86RMIgX56AZ8ZiMC62ce7mXFaIBECB8Ia5IePz6G/b86hP7BWf6xT1tA2knHepKOOL/flfAKEQB29gTxKESi6aeOEVQohF6UI2DpAKPUVEX4BaRpuPa7bCpBXex/EAEIgr3V5vVYCcghUVC/9ONNJe7IXjEPgOeORS4JAWQJ5u0zKfr7MdfgFlKHUzmu2tr6P1h8+OnW/sVVanXg6M/O2reyt54sQsI447C/QDVu+ALIGdCeWArqJ8LwMgaoBYcrkmXUNfgFZVNr9mu43Wnq0uVSUR8DlkMQIgbxW5fWBCGjgX1//JnebDksBA+EN9sN1eH2XgafZnWZ5JDcISADI6ri7tzfUAksMvGX+tKTkUkIIuNRajpQ1bxlAxWcpwJFGbFkxi/xL6i6qQlkrhDCp3QTUJ7a3f4y+23o+FAtALzoT4+PR2LmxeNlzYuLNVgciQgj0ak3eL01A63Ibj59kLgMok+mLF6K5uWvsxS5NlAtFYHPz27hfdfuX2KAjv4CPbn/Y6pu2jXq7kqccRPd2f2rt4F/EUZEJZS2Ymjrfuv6FEChqOd4rRaCXANDN9boRADKbkSBQlsAwzL3aGz41NVm2iFxnkYBm/PsHP0cH+weR+sKwzf51VlXWAt0Pp6cv1Jlt33khBPpGF/YH9SOVAJBZTo/zksys8/PvsSsgDxCvx/0nfSzsixfqW88bv/HjFzCczqj7h9p/3wz4svrov2b+RfeV4ZS0/m9VYCIdqS5BMMwzDBAC9bettznqh7lrzHIK0FE2VrdU7+jIyAkTdfbR0dfPT97w6IEGsiZvYv8yN0/NmmymF/HNOl/w2fzuJvLGL8Ae5b29n+LM0/1UM/z4ucVAUPZqVH/OiSCQGNXjphNCoGnijnyflPmBMcsl6lw/3CYHN0cwUUwPCGjpann5Jlarkm2ZDOy6XPeFJHU8HrL3flIm1/5rorRgTjhsOq4KQsC1nlJTeZOBvlulK3uf1uJqwkU2HhPQDCzvxtvr+Gt5hb9RcganHTNNmH+T33Zek6UH7O5r8t6TqV75kuwTUH+cm73aqE8VQsB+uzb+DekbQfLDTtbffDfxNg6bL4QABCBggUCToYsRAhYa0HaWiWmuY3A369KsudkmT/4QgAAEmiEgy8DS0mKutarOUiAE6qRZU17JQK/ZfIfpnnW3mgiTDQQgAIH2E1Dsgdu3lq0XFCFgHXH2F2jN7ejlUexsE5vrzYyetflsVrwKAQhAIFQCTRyDjRBooHdphp943x8y4DdAnK+AAAQg4AcBLRHc+fi21W2FCIGa+4q22Ckalsz6+hvGKVg1V4nsIAABCEBgiARsWwUQAgM2rjz0d82MX0F2NPCz135AoHwcAhCAAAQ6CMgq8Mmf7nS8VucThEAfNDXY69SrePDHga8PgnwEAhCAAASqELB5DgZCoGRLJIN/W4+8LFkNLoMABCAAAQcJ6PRWndtiIyEEelDd3vkx2nz6DI/+Hpx4GwIQgAAE7BGwuTyAEMhoN63762S9p5vPWPPP4MNLEIAABCDQPIFbJqZAXjjsQUqDEOiit7n5bbTx+Alxtbu48BQCEIAABIZLwFbYYYSAaVcF95EFQM5/eP0Pt6Pz7RCAAAQgkE1AxxTPzV7LfnOAV4MVAlr718CvP07VGqAH8VEIQAACEGiEwNiYnZDDwQgBzfR3d4/3+2/v7DTSaHwJBCAAAQhAoE4Cf/nzJ3VmF+fltRCQyX/H7Pf/wcz6ifBXe98hQwhAAAIQaJgAQqAE8MTkT5S/ErC4BAIQgAAEnCKw9MFiNDExXmuZvbAIyOwvZz8F+8HZr9b+QWYQgAAEINAiAgiBrsbQrF/b/Vjz7wLDUwhAAAIQ8JLA7OzV6PLMpVrr5qRFQIM/wX5q7QdkBgEIQAACDhCwcRKhU0JAzn9raw9w/HOgs1JECEAAAhCon0DQQuDx439GivhHggAEIAABCIRKIEghID+Ata8e4AQYaq+n3hCAAAQgcEIgKCGgaH8bxgqwaQ7+IUEAAhCAAAQgEEXBCAGJgJW79/AFoNdDAAIQgAAEUgTGx9+Mlpdupl4Z/GHrnAURAYM3KjlAAAIQgICfBLwXAogAPzsutYIABCAAgXoIeL00gAiop5OQCwQgAAEI+EvAWyGACPC301IzCEAAAhCoj4CXQiAtAsbGfhfTOjj4uT5q5AQBhwjoN3D27NmOEus3wm+iAwlPIBAsAS+FgGIEbJnDgnQDlCfkvdV/RHt7PwXbyFTcTQIjI7+NRkZG4sKPmv8jo8eP9YIG9nNjYx0Vq3J62Orqfc7T6KDHEwiES8A7ISABICFw5syZaHn5ZnyzXLn7JUIg3D7euponM/T04D4xfnwE6Nk3Tg/wdVdAZ2o8fPhN3dmSHwQg4CgBr4SAzg24a2IF/PrrryciQO2CEHC0dzpabG3FiWfs545n7MkgX2XGbqvqiqp592/3bGVPvhCAgIMEvBICn3+xEgcM6j5bGSHgYM9seZFlcTpnBnr9yXwvM/2oMd0npvw2Fl9+AZ/99fNI/0kQgAAEEgLeCIH1h49M6OBvo/nfX4+mpy8m9Yv/IwQ6cPCkAoHEjK/ZfGzKN4N+G2b2FapwcqmsZbt7eyfPeQABCEBABLwQAtvbO9Hq3+9Hs7NXo8szl061LELgFBJe6CKQzPA1yGt2L8e8bme8ro849VSnbOq0TRIEIACBNAEtZeq+d+Xdd9IvD/y40RDDiblzavJ8ND//XmbhEQKZWIJ+UTP9ZNAfk4m/ywPfJziJA61PdaIuEIBAPQS8EAJaEpAD1O1bH+ZSSXwHci/gDa8JaBvemBnotZ4vxz1XTfv9NJIcaL8wvjMkCEDAXQIarG3F/nBeCOgmt7LyZfTR7Q9znbTwkna38/dbcnXsZNBPnPn6zcvlzyW7aHAOdLkVKXuoBHQfk7k+PXE5PDyMvjNb5GXlOzz8pRY0+p6pqcnMZfVBvqCxpQE5P8nJIQ2qu+A4SHUT8e950pFl3i/qC/7VPL9GGvxlCdONgwQBCLhDQP5KN8wytwbnoqRJrgTB1vPviy7r+V6W4Oj5oRIXNCIE5CAoZbS48H5ukTCL5qJx+g2Z+mXi1w9FA393+FynK1dD4SUCVoxIPjAWMxIEIOAOgXQgvLKllthX9Nx+Q4Y7LQTkACjVVLRve23t64HVUtnG4Dq7BOTcp22hGvh9duwblCIiYFCCfB4CwyOQtf29TGn0u+83lL6zQkAmEe2HLtruIJWk4Ckkdwlo5p+sXRUJPndrWG/JEQH18iQ3CDRJYNC9/Pr9f/7F/1X2HXBWCGjdf2HhRqFJGGtAk1243u+avnghFgC91sjq/Va3c0MEuN1+lB4Cdz6+XWjhLkOoH+d4J4WAZvobG09yYwYIlm6KhFIt023ac41M/woGpcGfNf9q7YIIqMaLqyHQNgKa/OTFwala1qpxc5wUAlI8SkXe4URRq9p1hne9rU44vBo1+836PSiqpsQACQIQcJPArVvLmb5P8cT3VURQBc0rYyWtuixu6x5sddeABvki3wB1g0//+zNujC3/PdjqfC2vdq3FU8hg/R5IEICAuwRkDS0KiPcf//lpXLkqPgRVrAK27sVWhYBmQEXWAMKptvsHIRNY4v3f7pK2t3Sa/a+u3nf2ACHdeJR0iNOLV3EO9vcPoqOjo/ZCp2QQsERgZubtaG72Wm7uiRBY+MONUhYBZVRlHPRSCMg3QKYRUrsIqLPNzV3LNH+1q6TtLo3iZ6x99cApi5d2f8iK18v/Q3E/VD/99bsnut2tR+kgcJpA3rJAcmUiBJY+WCycBCfXJ///69P/KSWubd2brVkEZA3QUcPaMZCVdIOUEiK1h4ACZFw3AqD7aOj2lNCNkkjcrq09cMoKMGnWNOUAWmTBy6MvUbD59Fn0gxEFWAryKPG66wQkku98/NFJNWTt63aW1pZACeO//PmTk+vKPCi7c05CYHnpZpksK11jRQgIULITIOu4YURApTZq5GItA8gK0N2xG/lyj75E4le+AC45BGb9RvtpEtV5ff0RgcH6gcdnWk8gvSwgS5j6e/ekSev9mgikBUOZiik/ORL3SraEwP8DAAD//0TEoW0AACLBSURBVO2djY8WRbbG24hZyI65kIWrqKCggDveq14gQMRcN7t/926uN2KQCKvjMi6goKKiO8MF4xgwMvH20+MZzltvfXZXf9Q7TyUz/XZ3fZz+9UedOnWq6olf61BlDisrV6u19fXtXJf/eKI6ePDZZn/102vVnTvfbZ/jj3EJLC39vjp+7JVq37694wpSeOkPHz6sVlevVffu3y/mSnDvl5dfrZ5eWsoqM1hcXf1ndf/+D1nzZWYkMCaB1//zterAgf2NCB9culy/N1vvj5bp8pWPmt1TJ9/Uh6tHjx5Vu3btmjlm7vz1b++ah+b29+79t8rMey5SiwNP5FYE1tbWq5VPrs6JcubMqer27W+oBMyRGfcAFLSjR16sdu/ePa4gBZd+69aX1c1bXxR1BVAC8EEJfZy6XNRXt7+ubtz4vEsWTEsCkyHwlz+/08gCRffC+x807847/31+Rr7rNz5rjh898tLMcbwLT9WKgDSIZ07+tgMlIqQ8F6EIQOsBIGwZyiJwYP/+6tCh52kZiLxteMbX1u42CgA+DCWFIZQA4QHrH6yADCRQMgFdAWsFFw1cbVFDg2BP3agyK3xYEHbtetLbmtf5ulhpOVxx2hzPahEwuwTaCMQ04xLYt3dvdejwC9WB/X8YV5CJlv7jxkZt1fq+sWyVqPA++WT9MTr15szHq2/UsBKiq2Bzc7Pvopg/CfRC4EhtNZVWPlr9sG4j6OPYhyKAb6juasU341KtCJhxEV8HiaePmb8nrwjEaDPmRXF/ugTQVYAH/+DBZwYVEpXrxsZPTZm/4PePG9vl37v3uP8dLw3iQk74oOgXbztBph/S+sczvlGXW2oYQwkQVrhfly9/RGVAgHBbFIGT//XG9jcG3wOY8fGdWqr9a87WVgEJsIDBj0B3ua3WSjB8h86eOT1zXNLobchPYNKKAEyjMH0AEMNiEUBF+1ztR4BuA/1wx1wlngep1BF/piKvK3h5Xh7Uz09X8/qhQy80vg6pMrquA/LA9A+ZteOrK34Jx7XTrk1eVNY3rn9eiZKl4+A5gFIorSJ9LvY3lYFYUow3NQLiHyByaWUAfgLy3cEzrrsKEP/d/71QwdEwprES8hOYtCLw8co/qvX1u8KI2wUkgAf9cF3ZysP8o1mRP9jqJ89RqbfFBxlR2Ylnb0o+qPjv3fuhqQRR+Zfc8rddNxQ5jA7xBXzc8NFyhZBp05VOH6cyoGnwdwkE4FOD1rwZ8L68d+FideL4K3M+ARIXFoJ/1V1jb7z+H3LIu9XdDraIMe+xLV3oWBYfgZAWExKC50kgJwE4Pi4vn9jW0pE3KqDNR1t91GKZEGWmpCF/bTi5PmS2vC68f7G2zvxsO1UdO/ZyowxaTyYcpDKQAItRRyfgq3zxLKOydynZ6BY4XisKYjEIXYxr1J2ky6GMS156S0VA0+DvhSGAFw9/XbscSgcCv4BzZ09HDw/1Wfd0P2lXLlQGuhJk+qEIhCpffGPQdWYGHIc1AJbU2IBGypW/f+yMHpLFmTBwgopAABBPk0DJBPQkKDHXAa9nzItgC+ZQKVuclGNUBlJoMe5YBNoqwKjUpSs1VnYqArGkGI8ESCCKgDZpXq8n9sHEUSETpe9DZDpMmUKgzxTlwE8jNkAZwNAqBhKYKoHzb521tvhD8uJ9CL1vtjx8IwdoEbAR4zESIAErAfEuxsfo8pWPG+fH0KgBZARzJiYFMwO6GP70ztvm4Zl9mTwIk6mkKAMcejyDkTsTIxBSgFPFxfPu6y4oVhHA0MFF87JOvbmMTwJTIYBK++3z55rWyJVaCRBnSHPMs0te24dIFAtXGhzXZaUqA3Q49pHluTEJ5FQEoATAIdC3XoDt/ZPrn7RFwCe4XAC3JEACwxCQPk3bAl96zLNLmg8ufTgz/wPi6W4GWzqbJSGURudjS6/P8zcJjEUglyIg3WAhpdpXn6b6/MQyy+Is6BM8VhDGIwES6E5AWgxipjdzjOkesI0cCH2AXOb9mPJExtAYaonHLQkMRUB3ieGdMtcQiJUDXXSwnEPhRfApFz7rmCj5seXGxqMiEEuK8Uhg4gSkpYGWB8z0+PiY4eCzzzRLD5vH9b5t5EDIkgC/AvnI6bzwO3a0AeTFBC1ck8AkyP2xCMg7hfLR4BVFO1Uecx0eKgKpBBmfBEggSGD37t9tz2Xu89nBeGd4QfuCaU3QH0NbOjF52s7hWMrH06aEuPLlcRLom4B+9sXynWLlgnw2a5lvJAItAn3fVeZPAgtKQFrdMRWp7yMEPOYQwlBF7jPpawUlBj2sAr5pjmPyYBwSyEXApggg71BXmZQvSjK6GDCnwNNPL82tTihxZWvrmpNzIcucxEvdsmsglRjjk8DECEhFHWtaD33ETMe9UL+kr1sglNaGEtOy3vnue9spHiOBQQnsr5djl3UC/ufd95qKHAJgPRPfEEAREkr1rqd2zS1EJOdtW58y7+tSsOUVe6yzIiAaT2yBjEcCJJCPgB4qiFxNs76tJFEcbOfkmJhBse/7+PjmRtcfUck3ZmtaJGLSMA4J9EEg5l3JXa5LEdCOi7nL7KwI8KXNfUuYHwnEE7A5/4WUAW3udJWE1g+c9kKVuav1bioornJcx32LH7nS8DgJ5CYwJUUg5r1te/1UBNqSYzoSmAAB8Q0wRXFV0IgX4zAoDkshxyj059tGJ7TpEtDX4PM70PH4mwT6JDCGIuBS5KkI9HmnmTcJFEog9GGQytx2eSGnI0nri+f6YKVMJGSTDcfY5egiw+NDEhhDEXBZ2fuUhRaBIZ8qlkUCGQmEWutoqaNC39j4aa7UUIsdnssI4ig1l0F9wObdvLT0+2YYoy1+6jF2D6QSY/zcBPqsfF2yuhSBY8dejnJQdOXrO05FwEeH50hgogRiHYfQsr58+aO5SXpCHxU4LO2p5xxwzaRmG+aXOlQwhJbdAyFCPN83gSkpAiHlvQuLzoqAz2u4i2BMSwIk4CaQYn63vaOh9FAEMDzKtYyqOUkKFJNTp96MGiYFJcKVr75im9z6PH+TQN8EuioCru4zX7eeyyLg66bryqGzIuAa6tBVMKYnARJwEwhNCmSmNN9T34cIaWFJeHppycxme1/PXpiqBGBZ5KNHXmzGYm9naPlhszpYovEQCfRGwDVqBnNt/FIrtL53RISyKQO+98+mCMDadv6tc5Jl9i0VgexImSEJ9Eug7UdB9+mjRY4WRpugHflSlACUJSsi2oY92mSxrYRoi8djJNAHAbPCXlu/W9359rvq/+7dq86dPd2MwIkp11QGzHx1HjZFIPZ90fmk/KYikEKLcUlgAgRCZn2XiKbzYFtTo/TdpyoB2ioRq4joNK7r4nES6IuArrD1DJptugxQwUMZx/wcOl9TdpsiEHIMNvNI3acikEqM8UlgZAKuuQNixNLOg22cj6BM4IP466+/RvsEQC6zRYRjMd0b9BMAKYaxCGiFVYbUQgF++/y5KD8XU255/7DmwKmTb5qnm32bItBWabcWYDlIRcAChYdIYKoEdLcAKkl8UDBBUEqQSjk0csCWJ9LCURDDCmPLlfLM/GJaOea6B2Ye3CeBvgnIFNsySVcba4CWEcrAzZtfOIfmmoqAy09B59n1d2dFgC9q11vA9CQQT0D3FcocAVhECCubpQSYKPfs2V0dP/ZKSrLGtPna8qvRrSGXEoBCY7s4ZLrjJEEZmQQyEZDWOLqp8DyfPXM6+vl3ieAbOWMqAjGWM1c5scc7KwIoSPoMYwtlPBIggXYE9MqBemEgVOioWGMDPkTXr39WLdeVemxAGoSYoX+I51MCcN7XT4rzEsQkK/vcksCQBKQLDc8zgmtuDVMmVOhQtmMtZ5Je+8XEKsuStu02iyKAD8R7Fy7OTVrSViimIwESsBMQM6XZakBsfKCO17OPxVbUMFHGDH+yS+I/GlICkFr3v/py0x9GXzyeI4E+CIgiAOt3aqW+snK1ev3115LEkuc95yydIQGyKAIoRIQPFcjzJEAC7QjoFrTrfVuqx/6/UX94Uj9Y7SSyp7p9+5vGSmg/O3s0xuxpTl40mwP3SKBfAl18AmDNOnrkpaSuO3TbQdFPGZ7YlUA2RYDevV1vBdOTgJ+ANhPqOQHMVGhpwxHvwIH95qne92WegJiC0OKJcTq0WT9i8mccEshBoIsiAIV9be1u7VdwKloUKA/P1da92C6I6Iw9EbMpAq4WiqdsniIBEkggoL3s9ZhmVxZoieAjNkRA9yBmDNyouxtCASMfIFvshw55Y7ljBhIYg4C2xJnl49n0dcVJA1m/u2Ye5j587lKdeM08UvezKQK4YISVT66mysD4JEACEQRk/oCUivHA/v21Q+AJ78cqomhvFLTYYQlAH6ovwAKA9QtiFQCdl3aM1Mf5mwT6JuDzZYEvjO95lncVeaAbzKc04DrwDiFOKF7ua86qCMAUSctA7lvE/Ehgi4DPUdDHCH4DUAZyOwbiI7e6eq1aW99qBNhkQOsf3wV8LLuUz5EDNro8NhQBGUKoy4OzLRrAsG7pAMVYD+eV5bRjuhjaOCTqstv+zqIImFqMXHhboZiOBKZMAJWbOOPdv//DIKJqD+I2znNoYZw8+UanylhfKJSAb+vWELa2gEq/zWRHtrxwTC9y5IrD4yTQFwEZOaDzxwRDqPBNi4CpCMhEREgb4xyryxjqdzZFQD6MEJxWgaFuH8vpg4BU9HvqGft21+OAm2392xwTPKQTm55IqO37hXcUTktDmx1z3CN2DeSgyDzaErC15uG3YpvMC468cIKVoBV3dNX5hhOaSoTk0fc2iyJgjkeGhQDOTAwkMEUCmCscrVVUiNvbugW766l6v97GhiEVAf0h6mImH2K60lh+KfGoCKTQYtzcBLQijrzl3bdZCvB+6nUEUD9eunR5WySfVcCsS7cT9fwjiyJgk5HLh9qo8NhQBODp27WiD8kqH4NQvBzntSLQ1Uxu6+/MIWOfeVAR6JMu8w4RgDUNFbgEdImhjlv+46sz/gA4j0awjotjMk227uLDcR2gBCCkNEZ0+i6/e1MEOO1wl9vCtCECqOgRxCln396tufZlP5Q+x/mxFIGulaKMPsjBYKg8ul7zUHKynMUlYL43cBRE16FZceNZFcdeoSFWPN8wQnT5mY6Hkr7vbW+KwJAfyb4hMf9xCOhWPV42tPCHrOhDV637/kJxu56XNQbQEukyph7+D+ffOtdVnMHTUxEYHDkLNAj4KnEdFc+qaXVDJY86UXcZ6DT4Pcb8ASJDb4oACuDLK5i59RGAuQwVfFPR1y170ynPl3bMc22d9trILH2RMB9isaCUIH4QSAPLyZSUqdjr4LcklhTj9UXA9BNwlYNnVd5XiQMlAMH17uG9tg1FlPR9b3tVBMQc0vdFMP9yCMBRDy8DKqdSKyWhPYYiIGXvtC0VgZ12x6d3vWiooO8fW1cQR3lTEYAlz5cOQwyPHn1pe1iyK/++jveqCAz5oewLEPPtTgCe6v9eTyqzVFf+Zn9a99zHy2HI59v8sIx31bMloyXz8OHP9Qfsd8F7K3Mu/PjjRjP/AFpJSBezFDIVgVnu3BuHQKh7QLrEtXNvSFIoD9dqK58echhKk/t8r4qAQMktNPObNgG0+lHxo+WPWeV8mvC0r8Qv3U5XBKAEXKnXFzAnFYKHNeZeQHhQf+TwoXMF3zzuOg0VAU2Dv8ciEBp+K3VeiiLgmphoyGvsVRHAhfAFHvJ2jlsW+tBQ8eNvJ4Qhu76maBGAAgBlCMsOtw1UBNqSY7qxCPjmAYBMUHwfPHjo9AfQciPuxQ8+rP70ztv68OC/sygCvv4Pzicw+D0dtEC0/g8ffqFZNlPPLjmoECMV1nU8f4rYIZNkSl6544ppc339bnLWKQ5YyZkzAQn0QCCltR8qHha1/Qf+0CzGFYrb5/nOigDMg1iBybVsIucT6PP2jZc3+nYx5nWRTf8hukNau3J8fKCwo3/e5bkcut7QeZhFYSEQX4BQfJyPva4hWcfIzTg7l0CM02AMHYwSwBBk35DCmHxyxOmkCODDIlMJ2+ZchoBD9qPmAMI8/ARgAYB3K5aT3ckBz36X8fyp7GIrTFe+MN/jXTx+7OW5RVJcadoeR8MAZcGJMBSO1fKEniU0NvQUraE8eZ4E+iYQWjMgVD6+H7AoYt2BKThQd1IEVlauzixBCqvAoUPPzzCgIjCDo+gdmHGPH39lYZ3/Um6OOAWlpOkSN7Yv3SwDcq5+em3bYc+c6MSMn3Mf7/5XX31dbW5uOrON8X0YmrVTWJ6YFAE0SjAMWYIsEoZ9WL7adFVJXjFbmeQrJq4ZB+8GLAshJdhM19d+a0UAZo2VT67OyYUlGdHqEE9xKgJziIo70AzxssypXdyFZBR46C6vVEUA/farq9eqe/e3JjLBpfvmOc+IZiYrtHxg/rx168uZ47ITcrxCPCoCQmtnbfHMI0hXVuo04jK/f1/UUMe5LOG+MvE8450Yc7igKV9rRQBdAvjY2MJSPR3s8vKJxuRBRcBGqJxjGC7z2vKr24pdOZL3K+mQjoK4EnPRE9fV4Z28WVe6MM+bIcYMb6bJtd/IdfOL6s53389kac7JPnPytx1cC6waDItHAJU9KlS07GW5bz0TZpcrxrA883nrkp8r7aG6m/TokRejvpFQjGWZYmksu/Id8ngrRSBGQ8dFwtMZ/Xuu1sCQF8qy0gnEenSn51x2iqH9A4SWr9LEO4n3TFsAJJ1sh+wWkDLNrakQ+K5J0rIxISTK3MISBUW2j8reRySmnvKlTzmH+g4Wi63h03+wKgWoC9GdPhW/AH19rRSBlBcT1oGNGgBDWQS6OqeVdbVp0o7VQpVKE+XDEe+XunWxUfeF+ip/ubLcSh0+al2cnKAQwDzqGm0kcmOb8r3R6fh7GALSVy8t+2Zbf/ensGbIkHN9aNqiFEAJeuKJJ6pv63cW7+2YVjktn/m7d0XALJD70ydAJcB/j4buFhBpxLGuzcdN0kpeXbaoxNE1ONTcBjCl9u341YXHTkgrlb2Y7VHR7XqqNunXFf6UA6x371246HVYHUr+3Mp4TrlbKQIuR8GcgjGvcQikOqWNI+V4pQ5pbjSvUirz1BZy7nuqGQyhDPj8kUxG3O9GwGbGF2e9bjmPlxqKK5TJjY2fRhNiykoAoLRSBJCwb49MlMEwLAFo/efOnm7684YtuZzSMBNYjCm+jyuSSje1ayLGMz9FXrN8kSslj9i4Y/ljxMpXajxd4aNVv3tP3Yc/8dZ9V9boirpZO6z6hrN2LcNMDyUcE69NXZlqrQgA6o0bn5vXzf2CCXQZF1vwZUeLPvYzL102YpqPEVzSxMSNjWMzt/alDGjrQ6x8jPeYACoiGV8Pc/4U+u0fSzf8Lzy7mGNAglbqzXN4z2ImxZK8sIWCBYUKK61i4bVSpl1vrQjgotv0VSIdw/QI4AE+e+b09ASbiERmK3gMsXSlHmORwz3F9KVw3nKFtk5/NqWoD2UgtRvEdZ2LfpwVfv93GO/Ko18ezRUkfhNzJwo60EkRABhO/VnQ3faI2sdH3FNcUacwLA8V0thB9/WHxkjHdPOgxQNvZpgu24QL71+cazHlfo7oKDh7Z1jhz/LgXh4CnRQBiDD0DGt5Lpu5aAKoNMZeBlPLM5XfMEtfr7u/pjL8VSsCIYfdM2dOBft8RbnxKQIwl7osCi4ZYsqOvcc70VHQ5qG/0036sc8L47Uj0FkRwIcCSw2n9qW0E5ep+iCA9SFixnP3UfYU80RL2TU739jy6kmBXN0Dsa1yVLIHDz7jtQigSwTThruCrXsQisPJk28EFRFXnnJ80S2OmLobfchwJMs9q54w5JYEYgh0VgRQiKtlECMA44xPQPc9jy/NuBKgGwD931Bwpxh0JW/rHtDnffLLOxu695gz4WxtXXAFV2WdQxmwXZ9LjikfhyUHPNCXvFM89Kd8PyjbPIEsigCyXZSXdh7R4h8JVQaLT2BLmUU3AKwBUw56PDJkRateQqwSgPjyvobu/V//9m4VMvVLXiKHbJdq7+lTtWXA1bUg8VxbLPM8VYXMlJnmfJMI90sikE0RwAsLM+GYkzaUBH5KsoYqgynJmkMWtGI3H202q9o9qCtTDCeaih9A6PpQqaJ7QIKY5lOUALyrqGQRQvceikCo68hUSEQ2bNuu2y4WC53XFH7r8fdizp/6GPEpcKMM0yaQTRHAZeIDe/nyR4NO2DBtvGVIF6oMyriKeSnv3/+hevAAY4G3KntU+qVU+PNX8/iInu8BDo0YC+1z+HuccuuXHvoXmvscikDMyoc+p+E2z5fLymBeS1/7pnd+CdPp9sWC+S4+gayKAHDhozv2dI6Lf9vyXmGbD3VeCdrnhspeFt9BRf+wrvihkJZiUm5z5Vgausta5toTX6YtdskBRQAh1D0A3r453bXy4ipLjiMvyNj3PdTm/GZ1vLorg975che43UkEsisCgIcXGK2Or776mtaBAp6mKSsCYsaH+R7P1fZ2wSv70GOjRw+E4urzpsk9pAjIyIRQ9wDK8E3+k9J14ctHX0vsb1uFvwiTwMReP+ORQIhAL4qALhSmS1gJ0FpzBcSRgI89/QyExrDbxrO5bhUhYO5x9IE2v+stWkpbv7eGPDU7Lf+Z91juv7Toka2e+rNlMQudLGTSd128uVZCaB0C8UGA459v9ADKw321WQUwTO78W+dcIs0cRx5trQGs8GdQcocEogn0rghES2JEROWAMcx3vvveOMPdqRCAOVWUBZ9Mjem+bsEz5CMQ029vloZ36srfP545/Jc/vzOzb+6IIoDjIaUBcWyt+ZDVAekk2NLLOb2VPnzM6Y4heWzhazr8TQJpBCarCMhlQBm4dv0zdjEIEG5J4DcCKRUskpjWAD1ToQuqVgRirBCmVSCmS0HKtlkD0MpvFm+pLVJcNEdIcUsCeQlMXhGQy0VrZm19vekjhoOYGcQsKBN3iJkb3szsajBpcX8RCKQ4DdqsAXpOAhcPrQjEliet+phFj3S5tpEHKb4FOi/+JgESiCdQjCIQf0mzMdHKwMeMysAsF+4tBoEYcz3eAcwQCF8dHWJa+FoRQNpQVwLiiFXg1Kk3o6cZhmx6ciTkE2OxQDwGEiCBbgQWXhEAHnyYrtfdC/Q36PawMPX0CMS06m0tbVxJaEgg4piKQOwwQIz2QN99bDC7LZAutesjtizGIwESmCWwIxQBuWSYR2UeeQxD29zclFPckkCxBHxWAfjYrH56be7a0JUWs+KkqQjEKB5zhQUO2LotaA0IQONpEshIYEcpAi5uYsqkYuAixONTJuCqnNEqR0sbz7cZYvv79eRDyKPNaAWzbL0P2WzdFjHWCp0Pf5MACbQnQEXgN3Z62tX2OJmSBMYhYDOjm615LVmMfwDiy8yCOm3OSnpl5WrjBKzzdyk2Og5/kwAJ5CNARUCx9H04VbTRfsKDGmPydQsPZlUJcLh6+PBn2eV2BxHA0LqT9Up/OvgsXbGVuU0RSBkSqOUxf5uzHMp5X1eHxOGWBEggHwEqAoolTKlTXTSpzccX1/Poly2zMCoF7EuQqXpl3zYkU85xWwYBWysf9x0Krh41kzLTn00RiJllMETM1W0x5emuQ9fE8yRQKgEqAsadm6IygA/32TOnW6/rblxicLeZErpevEdCo1DUFQpCo1DUjpYSaIUQEuNvMXcGpgFGP74ZZGw/jsea3nGvZbliM78urXbka/MLgAPj2+fPDfacm9fEfRLYqQSoCFjuPD5UUxhuiA/j4cMvJC0xa7mcQQ81SsNvVggUrJWIZv+3xYO0UBzBoWl0++1rrUNpu7r6z+rwoReqAwf2BwuyefMjEZ7L15ZfjcrDVohtqCDixQ5NtOXJYyRAAu0JUBHwsMOH819r6xX6Moc0nWPo1L59e6vnDj5rbd15RF6YU6ZCoS+ssVjU9yZXQP+6Dpi3Hi3o27e/0YeL+X2wfm7gT9I1mH34GGmA6X6Rf9uAoYwY0miGNl1fZh7cJwESaEeAikALbrZKqjGZqz54ydbsi5fj2GLBHqzyJwEVEhQAhvEJuMbfjy9ZnAQ2f4G4lI9jYSTNRm3BgfUgxoLwOKX9l0sJGLrryy4dj5LAziVARWDn3nteuYcArA7mlLee6JM8NaV5+n3Dc21DHycJlEKRwIISoCKwoDeWl9WdwIX3LxY/HDNXN0EXmj7rCkcJdCHLtCSQhwAVgTwcmcsCEvh45R/V+vrd4q8MXU7LyydG8Te5fuPz2tfiaytDrE6I0TAMJEAC4xKgIjAuf5Y+YQI+c/aExXaKdqgeLXD40PODKATwmcGsgffuP57wSguGkQcpqxPqtPxNAiSQlwAVgbw8mdsCEUBlZk7GswiXh+4CeOmnrA6Yct0YdgjHQPhZ2AKVABsVHiOB8QhQERiPPUsuhMBqPfZ+EZewRpcBlIKDB5/JciegANy69aXTCoBCqARkQc1MSCArASoCWXEys0UlAIc3zC2Qay0HePRfu/7ZJJbCxkyEmGQICgFmJ0wJaPWvrd1t5tpwdQNIflQChAS3JDAtAlQEpnU/KM3ECaDiQ8v3Qb19WE/DjK2E2EmnZPIcc8IeyWesLZQAzBdw9MiLc34EmDtj89FmI9qD+rpR6YODy/xvXgOVAJMI90lgOgSoCEznXlCSHUjg+o3Pip3BMPZ2YaZMTElsWwMhNg/GIwES6I8AFYH+2DJnEggSWFSHRFw4rAAnjr/SaUriIEBGIAES6EyAikBnhMyABLoRgDLwwaUPs/kfdJMmT2qscHi8VgJSfQ7ylM5cSIAEUghQEUihxbgk0BMB9MFfvvzRJJwH214iLABYlOjo0ZfYDdAWItORwAgEqAiMAJ1FkoCNACwDWCa4tNkMsWgQJivCapm0ANjuLI+RwLQJUBGY9v2hdDuQAEYTwEKA4Fu9Euc2N7c8+cfAhGWJUfnnWJlwDPlZJgmQwBYBKgJ8EkhgAQnYlsqWy/SN98eQQFfA8EiY/5977tlm3gGOAnCR4nESKIsAFYGy7helJQESIAESIIGsBKgIZMXJzEiABEiABEigLAJUBMq6X5SWBEiABEiABLISoCKQFSczIwESIAESIIGyCFARKOt+UVoSIAESIAESyEqAikBWnMyMBEiABEiABMoiQEWgrPtFaUmABEiABEggKwEqAllxMjMSIAESIAESKIsAFYGy7helJQESIAESIIGsBKgIZMXJzEiABEiABEigLAJUBMq6X5SWBEiABEiABLISoCKQFSczIwESIAESIIGyCFARKOt+UVoSIAESIAESyEqAikBWnMyMBEiABEiABMoiQEWgrPtFaUmABEiABEggKwEqAllxMjMSIAESIAESKIsAFYGy7helJQESIAESIIGsBKgIZMXJzEiABEiABEigLAJUBMq6X5SWBEiABEiABLISoCKQFSczIwESIAESIIGyCFARKOt+UVoSIAESIAESyEqAikBWnMyMBEiABEiABMoiQEWgrPtFaUmABEiABEggKwEqAllxMjMSIAESIAESKIsAFYGy7helJQESIAESIIGsBKgIZMXJzEiABEiABEigLAJUBMq6X5SWBEiABEiABLISoCKQFSczIwESIAESIIGyCFARKOt+UVoSIAESIAESyEqAikBWnMyMBEiABEiABMoiQEWgrPtFaUmABEiABEggKwEqAllxMjMSIAESIAESKIsAFYGy7helJQESIAESIIGsBKgIZMXJzEiABEiABEigLAJUBMq6X5SWBEiABEiABLISoCKQFSczIwESIAESIIGyCFARKOt+UVoSIAESIAESyEqAikBWnMyMBEiABEiABMoiQEWgrPtFaUmABEiABEggKwEqAllxMjMSIAESIAESKIvA/wO6HpRgiKDHjwAAAABJRU5ErkJggg==',
                description: 'just something',
                parentMakeId: 2,
                parentMake: {
                  id: 2,
                  name: 'AUDI',
                  description: 'the is the one'
                }
          },
        ]
    }
}

export class Controller {
    protected headers: Headers;
    protected http: Http;
    protected url: string = environment.apiEndpoint;
    protected router: Router;
    errorService: any;
    overlayService: any;
    constructor(config: ControllerConfig, endpoint: string) {
        this.http = config.http;
        this.headers = config.headers;
        this.router = config.router;
        this.url += endpoint;
    }

    protected Get<T>(path: string = ''): Observable<T> {

        return this.http.get(this.url + path)// , { headers: this.headers })
            .map(response => {
                const data = (response.json());
                return data.result;
            })
    }

    protected Post<T>(path: string = '', body: any): Observable<T> {
        return this.http.post(this.url + path, body) // , { headers: this.headers })
            .map(response => {
                const data = (response.json());
                return data.result;
            })
    }
}

export class ControllerConfig {
    headers: Headers;
    http: Http;
    url: string;
    router: Router;

    constructor(http: Http, headers: Headers, url: string, router: Router) {
        this.headers = headers;
        this.http = http;
        this.url = url;
        this.router = router;
    }
}

export class UserController extends Controller {
    async GetInfo(): Promise<User> {
        return await this.Get<User>('').toPromise();
    }

    UpdateInfo(details: User, url: string): Observable<User> {
        return this.Post<User>(url, JSON.stringify(details));
    }

    GetAllUsers(): Observable<User[]> {
        return this.Get<User[]>(`/All`);
    }

    ToggleUserStatus(userId: number): Observable<User> {
        return this.Post<User>(`/toggle/${userId}`, {});
    }

    DeleteUser(userId: number): Observable<User> {
        return this.Post<User>(`/delete/${userId}`, {});
    }
}

export class BookingController extends Controller {

    CreateBooking(booking: Booking): Observable<Booking> {
        return this.Post<Booking>('', booking);
    }

    UpdateBooking(booking: Booking): Observable<Booking> {
        return this.Post<Booking>('/update', booking);
    }

    GetAllBookings(): Observable<Booking[]> {
        return this.Get<Booking[]>('');
    }
}
export class VehicleController extends Controller {

    CreateVehicle(vehicle: Vehicle): Observable<Vehicle> {
        return this.Post<Vehicle>('', vehicle);
    }

    UpdateVehicle(vehicle: Vehicle): Observable<Vehicle> {
        return this.Post<Vehicle>('/update', vehicle);
    }

    DeleteVehicle(vehicleId): Observable<Vehicle> {
        return this.Get<Vehicle>(`/delete/${vehicleId}`)
    }

    GetAllVehicle(): Observable<Vehicle[]> {
        return this.Get<Vehicle[]>('');
    }

    GetAllMakes(): Observable<Make[]> {
        return this.Get<Make[]>('/makes');
    }

    GetAllModelsByMake(makeId): Observable<Model[]> {
        return this.Get<Model[]>(`/models/${makeId}`);
    }

    GetAllModels(): Observable<Model[]> {
        return this.Get<Model[]>(`All`);
    }
    GetDashboardModels(): Observable<Model[]> {
        return this.Get<Model[]>(`/DashboardModels`);
    }

    ModelToFavourites(modelId: number): Observable<Model> {
        return this.Post<Model>(`ModelToFavourites/${modelId}`, {});
    }
}
