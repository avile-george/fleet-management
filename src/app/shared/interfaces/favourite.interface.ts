import { User } from './user.interface';
import { Model } from './model.interface';

export interface UserModel {
    userId?: number;
    user?: User;
    modelId: number;
    model: Model
}
