import { Make } from './make.interface';

export interface Model {
    id?: number;
    imageBase64string?: string;
    pricePerDay?: number;
    numberOfSeats?: number;
    name?: string;
    description?: string;
    parentMakeId?: number;
    parentMake: Make;
}
