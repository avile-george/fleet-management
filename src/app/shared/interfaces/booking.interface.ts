import { Vehicle } from './vehicle.interface';
import { User } from './user.interface';
import { Model } from './model.interface';

export interface Booking {
    id?: number;
    vehicle?: Vehicle;
    vehicleId?: number;
    model?: Model;
    modelId?: number;
    userId?: number;
    user?: User;
    createdDate?: Date;
    startDate?: Date;
    endDate?: Date;
    price?: number;
    isActive: boolean;
}
