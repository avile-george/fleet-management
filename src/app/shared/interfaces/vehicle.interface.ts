import { Model } from './model.interface';

export interface Vehicle {
      id?: number;
      regNumber?: string;
      modelId?: number;
      colour?: string;
      year?: number;
      model?: Model;
      isActive: boolean;
}
