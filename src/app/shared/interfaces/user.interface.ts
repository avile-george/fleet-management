import { UserModel } from './favourite.interface';

export interface User {
      id: number;
      lastName: string;
      firstName: string;
      iDNumber: number;
      favourites?: UserModel[];
}
