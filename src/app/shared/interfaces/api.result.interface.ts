export interface ApiResult<T> {
    error: string;
    warning: string;
    information: string;
    result: T;
}
