export interface Make {
    id?: number;
    name?: string;
    description?: string;
}
