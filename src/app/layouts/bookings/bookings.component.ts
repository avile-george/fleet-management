import {
  Component, OnInit,
  ChangeDetectionStrategy,
  ViewChild,
  TemplateRef
} from '@angular/core';
import {
  startOfDay,
  endOfDay,
  subDays,
  addDays,
  endOfMonth,
  isSameDay,
  isSameMonth,
  addHours
} from 'date-fns';
// tslint:disable-next-line:import-blacklist
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView
} from 'angular-calendar';
import { Booking } from 'app/shared/interfaces/booking.interface';
import * as moment from 'moment';
import { MatDialog } from '@angular/material';
import { BookingDialogComponent } from 'app/components/dialogs/booking-dialog/booking-dialog.component';
import { ApiService } from 'app/shared/services/api.service';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  grey: {
    primary: '#4d4d4d',
    secondary: '#b3b3b3'
  }
};

@Component({
  selector: 'app-bookings',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent {

  @ViewChild('modalContent')
  modalContent: TemplateRef<any>;

  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  viewDate: Date = new Date();

  modalData: {
    action: string;
    event: CalendarEvent;
  };
  bookings: Booking[] = [];

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = []

  activeDayIsOpen = true;

  constructor(private modal: NgbModal, public dialog: MatDialog, private ApiServ: ApiService) {
    this.GetBookings();
  }

  GetBookings() {
    this.ApiServ.Booking.GetAllBookings().subscribe(_bookings => {
      this.bookings = _bookings;
      this.convertBookingsToCalenderEvents();
    })
  }

  convertBookingsToCalenderEvents() {
    this.bookings.forEach((booking) => {
      const eventColor = {
        primary: booking.vehicle.colour ? booking.vehicle.colour : '#ad2121',
        secondary: booking.vehicle.colour ? booking.vehicle.colour : '#ad2121'
      };
      this.events.push({
        id: booking.id,
        start: new Date(booking.startDate),
        end: new Date(booking.endDate),
        title: this.buildEventTitle(booking),
        color: eventColor,
        actions: this.actions,
        allDay: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        },
        draggable: true
      })
    })
    this.refresh.next();
  }

  buildEventTitle(booking: Booking): string {
    if (booking.user && booking.user.firstName) {
      return booking.user.firstName + ' ' + booking.user.lastName + ', ' + booking.vehicle.model.parentMake.name
        + ' ' + booking.vehicle.model.name + ',  Start: ' +
        moment(booking.startDate).startOf('day').fromNow() + ', End: ' + moment(booking.endDate).endOf('day').fromNow();
    } else {
      return booking.vehicle.model.parentMake.name
        + ' ' + booking.vehicle.model.name + ',  Start: ' +
        moment(booking.startDate).startOf('day').fromNow() + ', End: ' + moment(booking.endDate).endOf('day').fromNow();
    }
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    if (action === 'Edited') {
      const _booking = this.bookings.find(booking => {
        return booking.id === event.id
      });

      const dialogRef = this.dialog.open(BookingDialogComponent, {
        data: _booking
      });

      dialogRef.afterClosed().subscribe(result => {
        // this.animal = result;
      });
    }
  }

  addEvent(): void {
    const dialogRef = this.dialog.open(BookingDialogComponent);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.vehicleId = result.vehicle.id;
        this.bookings.push(result);
        this.convertBookingsToCalenderEvents();
        this.refresh.next();
      }
    });
  }

}
