import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LayoutRoutes } from './layout.routing';
import { ChartsModule } from 'ng2-charts';
import { NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { BookingsComponent } from './bookings/bookings.component';
import { VehicleManagementComponent } from './vehicle-management/vehicle-management.component';
import { ComponentsModule } from 'app/components/components.module';
import { MaterialModule } from 'app/shared/modules/material.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompareVehiclesComponent } from './compare-vehicles/compare-vehicles.component';
import { RegisterComponent } from './register/register.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(LayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    MaterialModule,
    NgbModule,
    ComponentsModule,
    NgbModalModule,
    ToastrModule.forRoot(),
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  declarations: [
    BookingsComponent,
    VehicleManagementComponent,
    DashboardComponent,
    CompareVehiclesComponent,
    RegisterComponent,
  ]
})

export class LayoutModule {}
