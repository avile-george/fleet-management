import { Component, OnInit } from '@angular/core';
import { Make } from 'app/shared/interfaces/make.interface';
import { Model } from 'app/shared/interfaces/model.interface';
import { UserService } from 'app/shared/services/user.service';
import { ApiService } from 'app/shared/services/api.service';
import { UserModel } from 'app/shared/interfaces/favourite.interface';
import { BookingDialogComponent } from 'app/components/dialogs/booking-dialog/booking-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-compare-vehicles',
  templateUrl: './compare-vehicles.component.html',
  styleUrls: ['./compare-vehicles.component.scss']
})
export class CompareVehiclesComponent implements OnInit {

  makeList: Make[] = [];
  modelList: Model[] = [];
  modelList2: Model[] = [];
  FavouritesModels: UserModel[] = [];
  makeId: number;
  makeId2: number;
  selectedModel1: Model = null;
  selectedModel2: Model = null;
  constructor(
    private userServ: UserService,
    private ApiServ: ApiService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.FavouritesModels = this.userServ.currentUser.favourites ? this.userServ.currentUser.favourites : [];
    this.GetAllMakes();
  }

  AssignModel(event) {
    this.ApiServ.Vehicle.GetAllModelsByMake(event.value).subscribe(models => {
      if (event.source.id === 'makeone') {
        this.modelList = models;
      } else {
        this.modelList2 = models;
      }
    })
  }

  GetAllMakes() {
    this.ApiServ.Vehicle.GetAllMakes().subscribe(makes => {
      this.makeList = makes;
    })
  }

  getColor(modelId: number) {
    const _model = this.FavouritesModels.find((data => {
      return data.modelId === modelId;
    }))
    return _model ? '#0000e6' : '#ccccff'
  }

  ModelToFavourites(modelId) {
    this.ApiServ.Vehicle.ModelToFavourites(modelId).subscribe(model => {
      const index = this.FavouritesModels.findIndex(fav => {
        return fav.modelId === model.id;
      })

      if (index >= 0) {
        this.userServ.currentUser.favourites.splice(index, 1);
      } else {
        this.userServ.currentUser.favourites.push({model: model, modelId: model.id});
      }
    })
  }

  MakeBooking(model: Model): void {
    this.dialog.open(BookingDialogComponent, {data: {model: model, modelId: model.id}});
  }
}
