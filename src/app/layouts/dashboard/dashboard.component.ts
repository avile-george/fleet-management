import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { UserService } from 'app/shared/services/user.service';
import { ApiService } from 'app/shared/services/api.service';
import { Model } from 'app/shared/interfaces/model.interface';
import { UserModel } from 'app/shared/interfaces/favourite.interface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  modelsToDisplay: Model[] = [];
  FavouritesModels: UserModel[] = [];

  constructor(
    private userServ: UserService,
    private ApiServ: ApiService, ) { }

  ngOnInit() {
    this.GetFavouriteModels();
  }

  GetFavouriteModels() {
    this.FavouritesModels = this.userServ.currentUser.favourites ? this.userServ.currentUser.favourites : [];

    this.FavouritesModels.forEach(fav => {
      this.modelsToDisplay.push(fav.model);
    })

    if (this.modelsToDisplay.length < 8) {
      this.GetDashboardModels();
    }
  }

  GetDashboardModels() {
    this.ApiServ.TestData.forEach(model => {
      this.modelsToDisplay.push(model);
    });
    this.ApiServ.Vehicle.GetDashboardModels().subscribe(models => {
      models.forEach(model => {
        this.modelsToDisplay.push(model);
      });
    })
  }

  getColor(modelId: number) {
    const _model = this.FavouritesModels.find((data => {
      return data.modelId === modelId;
    }))
    return _model ? '#0000e6' : '#ccccff'
  }

  ModelToFavourites(modelId) {
    this.ApiServ.Vehicle.ModelToFavourites(modelId).subscribe(model => {
      const index = this.FavouritesModels.findIndex(fav => {
        return fav.modelId === model.id;
      })

      if (index >= 0) {
        this.userServ.currentUser.favourites.splice(index, 1);
      } else {
        this.userServ.currentUser.favourites.push({model: model, modelId: model.id});
      }
      this.GetFavouriteModels();
    })
  }
}
