import { Component, OnInit, ViewChild } from '@angular/core';
import { Vehicle } from 'app/shared/interfaces/vehicle.interface';
import { MatDialog, MatTableDataSource, MatPaginator } from '@angular/material';
import { ApiService } from '../../shared/services/api.service';
import { VehicleDialogComponent } from '../../components/dialogs/vehicle-dialog/vehicle-dialog.component';

@Component({
  selector: 'app-vehicle-management',
  templateUrl: './vehicle-management.component.html',
  styleUrls: ['./vehicle-management.component.scss']
})
export class VehicleManagementComponent implements OnInit {
  displayedColumns: string[] = ['Image', 'RegNumber', 'Colour', 'Year', 'PricePerDay', 'NumberOfSeats', 'ModelName', 'MakeName', 'Manage'];
  vehicles = new MatTableDataSource<Vehicle>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(public dialog: MatDialog, private ApiServ: ApiService) { }

  ngOnInit() {
    this.vehicles.paginator = this.paginator;
    this.GetAllVehicles();
  }

  GetAllVehicles() {
    this.ApiServ.Vehicle.GetAllVehicle().subscribe(vehicles => {
      this.vehicles.data = vehicles;
    })
  }

  applyFilter(filterValue: string) {
    this.vehicles.filter = filterValue.trim().toLowerCase();
  }

  OpenVehicleDialog(vehicle: Vehicle) {
    const dialogRef = this.dialog.open(VehicleDialogComponent, {
      data: vehicle,
      width: '600px',
      height: '550px'
    });

    dialogRef.afterClosed().subscribe(v => {
      if (v && !vehicle) {
        this.vehicles.data.push(v);
        this.vehicles._updateChangeSubscription();
      }
    })
  }

  DeleteVehicle(vehicle: Vehicle) {
    this.ApiServ.Vehicle.DeleteVehicle(vehicle.id).subscribe(result => {
      if (result) {
        alert('Deleted Successfully');
      }
    })
  }
}
