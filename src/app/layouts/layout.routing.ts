import { Routes } from '@angular/router';

import { BookingsComponent } from './bookings/bookings.component';
import { VehicleManagementComponent } from './vehicle-management/vehicle-management.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CompareVehiclesComponent } from './compare-vehicles/compare-vehicles.component';

export const LayoutRoutes: Routes = [
    { path: 'dashboard',      component: DashboardComponent },
    { path: 'bookings',  component: BookingsComponent },
    { path: 'vehicle-management',  component: VehicleManagementComponent },
    { path: 'compare',  component: CompareVehiclesComponent },
];
